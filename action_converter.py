# -*- coding: utf-8 -*-

# import math
#Create logging file action_converter.log
import logging
logging.basicConfig(filename='action_converter.log', encoding='utf-8', level=logging.DEBUG)


import os
import pcbnew
import re
import wx
import wx.lib.scrolledpanel

if __name__ == '__main__':
    from core import Core, ELT
    from transformer import Transformer, Coil, DesignParameter
    import util
else:
    from .core import Core, ELT
    from .transformer import Transformer, Coil, DesignParameter
    from . import util


class ConverterDialog(wx.Dialog):

###Design User Interface
    def __init__(self, parent, board: pcbnew.BOARD, footprint: pcbnew.FOOTPRINT, *args, **kw):
        super(ConverterDialog, self).__init__(
            parent, id=wx.ID_ANY, title=u"Transformer",
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER, *args, **kw)

        ##2 Panels in Dialog
        self.board = board
        self.footprint = footprint
        #Parameters panel
        parameters_panel = wx.Panel(self)
        #Configuration panel, scrollable
        self.pcb_configuration_panel = wx.lib.scrolledpanel.ScrolledPanel(self)
        self.pcb_configuration_panel.SetupScrolling()

        vbox = wx.BoxSizer(wx.VERTICAL)


        self.layer = [self.board.GetLayerName(x) for x in self.board.GetEnabledLayers().CuStack()]
        #Log enabled Layers
        print("Enabled Layers: %s", self.layer)

        ##Panel 1
        #Grouping of Transformer Parameters
        sb1 = wx.StaticBox(parameters_panel, label='Transformer Parameters')
        sbs1 = wx.StaticBoxSizer(sb1, orient=wx.VERTICAL)

        #Drop-Down-Menue Core Type 
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        hbox1.Add(wx.StaticText(parameters_panel, label='Core Type'))
        core_types = ['EL','ELT']
        self.core_sizes = ['11X4','13X4.4','15.5X5.8','18X7.3']
        self.core_type_combo_box = wx.ComboBox(parameters_panel, choices=core_types)
        hbox1.Add(self.core_type_combo_box)
        self.core_size_combo_box = wx.ComboBox(parameters_panel, choices=self.core_sizes)
        hbox1.Add(self.core_size_combo_box)
        sbs1.Add(hbox1, flag=wx.ALL | wx.EXPAND, border=5)

        #Textbox Number of PCBs
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        hbox2.Add(wx.StaticText(parameters_panel, label='Number of PCBs'))
        self.pcb_count_text_control = wx.TextCtrl(parameters_panel, style=wx.TE_PROCESS_ENTER)
        hbox2.Add(self.pcb_count_text_control, flag=wx.RIGHT, border=30)
        sbs1.Add(hbox2, flag=wx.ALL | wx.EXPAND, border=5)
        
        #Read only Textbox Number of Layers per PCB 
        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        hbox3.Add(wx.StaticText(parameters_panel, label='Number of Layers per PCB'))
        self.layer_count_text_control = wx.TextCtrl(parameters_panel, value=f'{len(self.layer)}', style=wx.TE_PROCESS_ENTER |wx.TE_READONLY)
        hbox3.Add(self.layer_count_text_control)
        sbs1.Add(hbox3, flag=wx.ALL | wx.EXPAND, border=5)

        #Textbox Number of Windings Primary
        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        hbox4.Add(wx.StaticText(parameters_panel, label='Number of Windings Primary'))
        self.primary_windings_count_text_control = wx.TextCtrl(parameters_panel, style=wx.TE_PROCESS_ENTER)
        hbox4.Add(self.primary_windings_count_text_control, flag=wx.RIGHT, border=30)
        sbs1.Add(hbox4, flag=wx.ALL | wx.EXPAND, border=5)

        #Textbox Number of Windings Secondary
        hbox5 = wx.BoxSizer(wx.HORIZONTAL)
        hbox5.Add(wx.StaticText(parameters_panel, label='Number of Windings Secondary'))
        self.secondary_windings_count_text_control = wx.TextCtrl(parameters_panel, value='2', style=wx.TE_PROCESS_ENTER |wx.TE_READONLY)
        hbox5.Add(self.secondary_windings_count_text_control, flag=wx.RIGHT, border=30)
        sbs1.Add(hbox5, flag=wx.ALL | wx.EXPAND, border=5)
        
        #Textbox Track Width Primary
        hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        hbox6.Add(wx.StaticText(parameters_panel, label='Track Width Primary'))
        self.primary_track_width_text_control = wx.TextCtrl(parameters_panel, style=wx.TE_PROCESS_ENTER)
        hbox6.Add(self.primary_track_width_text_control, flag=wx.RIGHT, border=5)
        hbox6.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox6, flag=wx.ALL | wx.EXPAND, border=5)
        
        #Textbox Clearance Primary
        hbox7 = wx.BoxSizer(wx.HORIZONTAL)
        hbox7.Add(wx.StaticText(parameters_panel, label='Clearance Primary'))
        self.primary_clearance_text_control = wx.TextCtrl(parameters_panel, style=wx.TE_PROCESS_ENTER)
        hbox7.Add(self.primary_clearance_text_control, flag=wx.RIGHT, border=5)
        hbox7.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox7, flag=wx.ALL | wx.EXPAND, border=5)
        
        #Textbox Track Width Secondary
        hbox8 = wx.BoxSizer(wx.HORIZONTAL)
        hbox8.Add(wx.StaticText(parameters_panel, label='Track Width Secondary'))
        self.secondary_track_width_text_control = wx.TextCtrl(parameters_panel, style=wx.TE_PROCESS_ENTER |wx.TE_READONLY)
        hbox8.Add(self.secondary_track_width_text_control, flag=wx.RIGHT, border=5)
        hbox8.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox8, flag=wx.ALL | wx.EXPAND, border=5)
        
        #Textbox Clearance Secondary
        hbox9 = wx.BoxSizer(wx.HORIZONTAL)
        hbox9.Add(wx.StaticText(parameters_panel, label='Clearance Secondary'))
        self.secondary_clearance_text_control = wx.TextCtrl(parameters_panel, value='-', style=wx.TE_PROCESS_ENTER |wx.TE_READONLY)
        hbox9.Add(self.secondary_clearance_text_control, flag=wx.RIGHT, border=5)
        hbox9.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox9, flag=wx.ALL | wx.EXPAND, border=5)

        #Read only Textbox calculated primary track
        hbox32 = wx.BoxSizer(wx.HORIZONTAL)
        hbox32.Add(wx.StaticText(parameters_panel, label='Suggested Track Width Primary'))
        self.calculated_primary_track_width = wx.TextCtrl(parameters_panel, style=wx.TE_READONLY)
        hbox32.Add(self.calculated_primary_track_width)
        hbox32.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox32, flag=wx.ALL | wx.EXPAND, border=5)

        #Read only Textbox calculated primary clearance
        hbox33 = wx.BoxSizer(wx.HORIZONTAL)
        hbox33.Add(wx.StaticText(parameters_panel, label='Suggested Clearance Primary'))
        self.calculated_primary_sep = wx.TextCtrl(parameters_panel, style=wx.TE_READONLY)
        hbox33.Add(self.calculated_primary_sep)
        hbox33.Add(wx.StaticText(parameters_panel, label='mm'))
        sbs1.Add(hbox33, flag=wx.ALL | wx.EXPAND, border=5)
        
        parameters_panel.SetSizer(sbs1)


        ##Panel 2
        #Grouping of PCB Configuration
        self.sb2 = wx.StaticBox(self.pcb_configuration_panel, label='PCB Configuration')
        self.sbs2 = wx.StaticBoxSizer(self.sb2, orient=wx.VERTICAL)       
        self.pcb_configuration_panel.SetSizer(self.sbs2)


        ##Ok & Cancel Button & Calculate Button
        hbox30 = wx.BoxSizer(wx.HORIZONTAL)
        drawButton = wx.Button(self, label='Draw')
        closeButton = wx.Button(self, label='Close')
        calculateButton = wx.Button(self, label='Calculate')
        hbox30.Add(drawButton)
        hbox30.Add(closeButton, flag=wx.LEFT, border=5)
        hbox30.Add(calculateButton, flag=wx.LEFT, border=5)

        vbox.Add(parameters_panel, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(self.pcb_configuration_panel, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(hbox30, flag=wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        #Bind events to buttons
        self.core_type_combo_box.Bind(wx.EVT_COMBOBOX, self.CoreType)
        self.core_size_combo_box.Bind(wx.EVT_COMBOBOX, self.CoreSize)
        self.layer_count_text_control.Bind(wx.EVT_TEXT_ENTER, self.LayerCount)
        self.pcb_count_text_control.Bind(wx.EVT_TEXT_ENTER, self.OnKeyTyped)
        self.primary_windings_count_text_control.Bind(wx.EVT_TEXT_ENTER, self.PrimaryWindings)
        self.secondary_windings_count_text_control.Bind(wx.EVT_TEXT_ENTER, self.SecondaryWindings)
        self.primary_track_width_text_control.Bind(wx.EVT_TEXT_ENTER, self.PrimaryTrackWidth)
        self.primary_clearance_text_control.Bind(wx.EVT_TEXT_ENTER, self.PrimaryClearance)
        self.secondary_track_width_text_control.Bind(wx.EVT_TEXT_ENTER, self.SecondaryTrackWidth)
        self.secondary_clearance_text_control.Bind(wx.EVT_TEXT_ENTER, self.SecondaryClearance)
        calculateButton.Bind(wx.EVT_BUTTON, self.on_calculate)
        drawButton.Bind(wx.EVT_BUTTON, self.on_draw)
        closeButton.Bind(wx.EVT_BUTTON, self.on_close)


        #Create primary DesignParameter() object to store design parameters for primary side
        self.primary_design_parameters = DesignParameter(board = self.board)

        #Create secondary DesignParameter() object to store design parameters for secondary side
        self.secondary_design_parameters = DesignParameter(board = self.board)

        #Create primary Coil() object to store coil parameters for primary side
        self.primary_coil = Coil(board = self.board, design_parameter = self.primary_design_parameters)

        #Create secondary Coil() object to store coil parameters for secondary side
        self.secondary_coil = Coil(board = self.board, design_parameter = self.secondary_design_parameters)

        
        footprint_value = footprint.GetValue()
        footprint_properties = footprint.GetTypeName()
        print(footprint_properties)

        match = re.match("[A-Z]*", footprint_value)
        if match:
            core_type = match.group(0)
        else:
            raise ValueError(
                'Module value badly formatted: no core type detected')

        match = re.match("[0-9X.]*", footprint_value[match.end():])
        if match:
            core_size = match.group(0)
        else:
            raise ValueError(
                'Module value badly formatted: no core size detected')

        self.trafo = Transformer(self.board, self.footprint, core_type, core_size)

        self.trafo.add_primary(self.primary_coil)
        self.trafo.add_secondary(self.secondary_coil)
        
        #Get Nets on board by name
        self.nets =[self.board.GetNetsByName().__getitem__(x) for x in self.board.GetNetsByName()]
        self.net_names = [self.board.GetNetsByName().__getitem__(x).GetNetname() for x in self.board.GetNetsByName()]
        logging.info(self.trafo)
        print (self.trafo)


    def OnKeyTyped(self, e):
        
        #Delete existing PCB configuration if there
        try: self.pcbnum  
        except AttributeError:
            self.pcbnum = None

        #if self.pcbnum is not None:
            #if delete:
            #for x in range(0,  self.pcbnum):
                #Create list with needed number of hbox variables
                #for item in self.hbox:
                #    window = item.GetItem(item).GetWindow()
                #    window.Destroy()
                #Create list with needed number of layerhbox variables
                #self.layerhbox.Hide()
                #self.layerhbox.Destroy()
                #Create list with needed number of layervbox variables
                #self.layervbox.clear()
                #Creat list with the needed number of combobox variables
                #self.pnl2.Delete(combobox)

        self.pcbnum = int(self.pcb_count_text_control.GetValue())

        #Set PCB count in self.trafo.core object
        self.trafo.core.pcb_count = self.pcbnum
        #Log number of PCBS
        print("Number of PCBs: %i", self.pcbnum)
 
        #Get nets to choose from
        side = self.net_names

        #Create list with needed number of hbox variables
        self.hbox = []
        for x in range(10, 10+self.pcbnum):
            self.hbox.append(f'hbox{x+1}')     

        #Create list with needed number of layerhbox variables
        self.layerhbox = []
        for x in range(2, len(self.layer)*self.pcbnum+3):
            self.layerhbox.append(f'layerhbox{x+1}')
        
        #Create list with needed number of layervbox variables
        self.layervbox = []
        for x in range(0, self.pcbnum+1):
            self.layervbox.append(f'layervbox{x+1}')
        
        #Creat list with the needed number of combobox variables
        self.combobox = []
        for x in range(0, len(self.layer)*self.pcbnum):
            self.combobox.append((wx.ComboBox(self.pcb_configuration_panel, choices = side)))

        #Create menue for PCB configuration
        x = 0    
        for i in range(0, self.pcbnum):
            self.hbox[i] = wx.BoxSizer(wx.VERTICAL)
            self.hbox[i].Add(wx.StaticText(self.pcb_configuration_panel, label=f'PCB{i+1}'))
            self.layervbox[i] = wx.BoxSizer(wx.VERTICAL)
            for j in range(0, len(self.layer)):
                self.layerhbox[x] = wx.BoxSizer(wx.HORIZONTAL)
                self.layerhbox[x].Add(wx.StaticText(self.pcb_configuration_panel, label=self.layer[j]))
                #Bind event to combobox
                self.combobox[x].Bind(wx.EVT_COMBOBOX, self.PCBConfiguration)
                self.layerhbox[x].Add(self.combobox[x]) 
                self.layervbox[i].Add(self.layerhbox[x], flag=wx.TOP, border=5)
                x = x + 1
            self.hbox[i].Add(self.layervbox[i], flag=wx.LEFT, border=15)
            self.sbs2.Add(self.hbox[i]) 

        self.Layout()
          

    def CoreType(self, e):
        #Get value of core type dropdown menu input
        self.core_type = self.core_type_combo_box.GetValue()
        #Set core size in self.trafo object
        #self.trafo.set_core_type(self.core_type)
        #Adjust core size combo box choices to chosen core type
        core_sizes_elt = ['18X7.3']
        core_sizes_el = ['11X4','13X4.4','15.5X5.8']
        if self.core_type == 'ELT':
            self.core_size_combo_box.Clear()
            self.core_size_combo_box.Append(core_sizes_elt)
        else:
            self.core_size_combo_box.Clear()
            self.core_size_combo_box.Append(core_sizes_el)

    def CoreSize(self, e):
        #Get value of core size dropdown menu input
        self.core_size = self.core_size_combo_box.GetValue()
        #Create self.trafo object with according core
        self.trafo = Transformer(self.board, self.footprint, self.core_type, self.core_size)
        #Set secondary track width
        secondary_track_width = self.trafo.core.winding_width
        self.secondary_track_width_text_control.SetValue(str(secondary_track_width))

    def LayerCount(self, e):
        #Get value of PCB layer count dropdown menu input
        self.layer_count_per_pcb = int(self.layer_count_text_control.GetValue())
        #Set PCB layer count in self.trafo object
        self.trafo._pcb_layer_count = self.layer_count_per_pcb
        #Set PCB layer count in self.trafo.core object
        self.trafo.core.layer_count_per_pcb = self.layer_count_per_pcb
        #Get enabled layers as list of properties of util.Layer class and set it in self.trafo.core object
        self.trafo.core.enabled_cu_layers = self.trafo.get_layer_properties()

    def PrimaryWindings(self, e):
        #Get value of primary windings textbox input
        self.primary_windings = int(self.primary_windings_count_text_control.GetValue())
        #Set primary windings count in primary_coil object
        self.primary_coil.set_winding_count(self.primary_windings)
        #Set primary windings count in self.trafo.core object
        self.trafo.core.primary_windings_count = self.primary_windings
        
    def SecondaryWindings(self, e):
        #Get value of secondary windings textbox input
        self.secondary_windings = int(self.secondary_windings_count_text_control.GetValue())
        #Set secondary windings count in secondary_coil object
        self.secondary_coil.set_winding_count(self.secondary_windings)
        
    def PrimaryTrackWidth(self, e):
        #Get value of primary track width textbox input
        self.primary_track_width = float(self.primary_track_width_text_control.GetValue())
        #Set primary track width in primary_design_parameters object
        self.primary_design_parameters.set_track_width_mm(self.primary_track_width)
        #Set primary track width in self.trafo.core object
        self.trafo.core.primary_track_width = self.primary_track_width

    def PrimaryClearance(self, e):
        #Get value of primary track to track clearance textbox input
        self.primary_copper_sep = float(self.primary_clearance_text_control.GetValue())
        #Set primary track to track clearance in primary_design_parameters object
        self.primary_design_parameters.set_copper_clearance_mm(self.primary_copper_sep)
        #Set primary track to track clearance in self.trafo.core object
        self.trafo.core.primary_track_track_sep = self.primary_copper_sep
      
    def SecondaryTrackWidth(self, e):
        #Get value of secondary track width textbox input
        self.secondary_track_width = float(self.secondary_track_width_text_control.GetValue())  
        #Set secondary track width in secondary_design_parameters object
        self.secondary_design_parameters.set_track_width_mm(self.secondary_track_width)

    def SecondaryClearance(self, e):
        #Get value of secondary track to track clearance textbox input
        self.secondary_copper_sep = float(self.secondary_clearance_text_control.GetValue())     
        #Set secondary track to track clearance in secondary_design_parameters object
        self.secondary_design_parameters.set_copper_clearance_mm(self.secondary_copper_sep)

    def PCBConfiguration(self, e):
        #Create list with PCB configuration inputs
        self.pcbs_config = []
        for x in range(0, len(self.combobox)):
            #Get value of all the dropdown menus of the PCB layer configuration and append to list
            type = self.combobox[x].GetValue()
            self.pcbs_config.append(type)
            #Set configuration in self.trafo object
            self.trafo.set_pcb_config(self.pcbs_config)
            #Get the PCB configuration in form of netcode and set it in self.trafo.core object
            self.trafo.core.layers_netcode = self.trafo.set_pcb_config_to_net_code()
            #Get primary layers count of chosen configuration and set it in self.trafor.core object
            self.trafo.core.primary_layers_count = self.trafo.get_primary_layer_count()
 
    def on_calculate(self,e):
        #Calculate biggest possible primary track width 
        self.trafo.core.calculate_dimensions()
        suggested_primary_track_width = self.trafo.core.primary_track_width
        self.calculated_primary_track_width.SetValue(str(suggested_primary_track_width))
        
        #Suggest min copper clearance as primary clearance
        suggested_primary_sep = DesignParameter(board = self.board).copper_sep_mm
        self.calculated_primary_sep.SetValue(str(suggested_primary_sep))

    def on_close(self, e):
        self.Destroy()              

    def on_draw(self, e):
        # parameter = DesignParameter()
        # print(parameter)
        #TODO this doesnt work
        primary_net_class = self.primary_design_parameters.create_new_net_class(net_class_name="primary")
        #self.primary_design_parameters.assign_net_class(self.nets[1])
        self.nets[1].SetNetClass(primary_net_class)

        # coil = Coil(winding_count=6)
        # print(coil)

        # print(self.trafo.core)
        # print(self.trafo)
        self.trafo.core.load_dimensions()
        self.trafo.core.calculate_dimensions()
        self.trafo.core.draw()
        #self.trafo.core.panelize_pcbs()
        self.Destroy()
 


class Converter(pcbnew.ActionPlugin):
    """
    Create a converter layout
    """

    def defaults(self):
        self.name = "Converter"
        self.category = "Create"
        self.description = "Create flat PCB converter layout"
        self.icon_file_name = os.path.join(os.path.dirname(__file__),
                                           'converter.svg.png')

    def Run(self):

        logging.info("converter: run")
        print("converter: run")

        # load board
        board = pcbnew.GetBoard()

        # TODO: check if there are any nets on the board, otherwise this will
        #       all fail!

        # Get PCB Editor frame

        self._frame = wx.FindWindowByName("PcbFrame")

        selected_footprints = [x for x in board.GetFootprints() if x.IsSelected()]
        n_selected_footprints = len(selected_footprints)

        # show a message box if no or more than one footprints were selected
        if n_selected_footprints == 0:
            caption = "Converter"
            message = "No footprint selected."

            dlg = wx.MessageDialog(self._frame, message, caption,
                                   wx.OK | wx.ICON_INFORMATION)

            dlg.ShowModal()
            dlg.Destroy()
            return -1
        else:
            if n_selected_footprints > 1:
                caption = "Converter"
                message = "Too many footprints selected."

                dlg = wx.MessageDialog(self._frame, message, caption,
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                dlg.Destroy()
                return -2

        # use the one selected footprint to design the converter geometry
        footprint = selected_footprints[0]


        dlg = ConverterDialog(self._frame, board, footprint)
        dlg.ShowModal()
        dlg.Destroy()

        logging.info('pcbnew: refresh')
        print('pcbnew: refresh')
        pcbnew.Refresh()
        logging.info('pcbnew: refreshed')
        print('pcbnew: refreshed')
