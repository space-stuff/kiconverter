# -*- coding: utf-8 -*-
import logging

import pcbnew

if __name__ == '__main__':
    import util
    from core import ELT
else:
    from . import util
    from .core import ELT


class DesignParameter():
    '''PCB design parameters
    '''
    def __init__(self, board, copper_sep_mm = 0.15, mill_sep_mm=0.2, mill_tol_mm=0.1, track_width_mm=0.15, via_width_mm=0.4, via_drill_mm=0.2):
        #Get preset desgin constraints 
        self.board = board
        design_settings = self.board.GetDesignSettings()
        #Copper
        #Minimum clearance
        self.copper_sep_mm = pcbnew.ToMM(design_settings.m_MinClearance)
        #Minimum track width
        self.track_width_mm = pcbnew.ToMM(design_settings.m_TrackMinWidth)
        #Minimum annular width (via)
        self.via_width_mm = pcbnew.ToMM(design_settings.m_ViasMinAnnularWidth)
        #Minimum via diameter
        self.via_drill_mm = pcbnew.ToMM(design_settings.m_ViasMinSize)
        #Copper to hole clearance
        copper_hole_sep = pcbnew.ToMM(design_settings.m_HoleClearance)
        #Copper to edge clearance
        self.mill_sep_mm = pcbnew.ToMM(design_settings.m_CopperEdgeClearance)
        #Holes
        #Minimum through hole
        self.min_through_hole = pcbnew.ToMM(design_settings.m_MinThroughDrill)
        #Hole to hole clearance
        hole_hole_sep = pcbnew.ToMM(design_settings.m_HoleToHoleMin)

        #The following parameters are not callable through KiCad and needs to be supplied directly
        #Milling tolerance
        self.mill_tol_mm = mill_tol_mm


    def __str__(self):
        return(
            f'Design parameter:'
            f'\n- copper separation = {self.copper_sep_mm}mm'
            f'\n- mill separation = {self.mill_sep_mm}mm'
            f'\n- mill tolerance = {self.mill_tol_mm}mm'
            f'\n- via drill = {self.via_drill_mm}mm'
            f'\n- via diameter = {self.via_width_mm}mm'
        )


    def set_track_width_mm(self, new_track_width_mm):
        """Set track width to new value (in mm)
        """
        if new_track_width_mm < self.track_width_mm:
            raise ValueError('Value must at least be minimum track width')
       
        self.track_width_mm = new_track_width_mm

    def set_copper_clearance_mm(self, new_copper_clearance_mm):
        """Set copper clearance to new value (in mm)
        """
        if new_copper_clearance_mm < self.copper_sep_mm:
            raise ValueError('Value must at least be minimum copper seperation')
        
        self.copper_sep_mm = new_copper_clearance_mm

    def create_new_net_class(self, net_class_name):
        """Create new net class with given parameters
        """
        self.net_class = pcbnew.NETCLASSPTR("{}".format(net_class_name))
        self.net_class.SetClearance(pcbnew.FromMM(self.copper_sep_mm))
        self.net_class.SetTrackWidth(pcbnew.FromMM(self.track_width_mm))
        self.net_class.SetViaDiameter(pcbnew.FromMM(self.via_drill_mm))
        self.net_class.SetViaDrill(pcbnew.FromMM(self.min_through_hole))
        self.board.GetNetClasses().Add(self.net_class)

        return self.net_class

    def assign_net_class(self, net):
        """Assign net a net class
        """
        net.SetNetClass(self.net_class)


    #Error messages for wrong inputs
    @property
    def copper_sep(self):
        return self._copper_sep

    @copper_sep.setter
    def copper_sep(self, sep):
        if not isinstance(sep, int):
            raise TypeError('Argument must be of type int')
        if sep <= 0:
            raise ValueError('Argument must be greater than zero')
        self._copper_sep = sep

    @property
    def copper_sep_mm(self):
        return pcbnew.ToMM(self._copper_sep)

    @copper_sep_mm.setter
    def copper_sep_mm(self, sep):
        if not isinstance(sep, float):
            raise TypeError('Argument must be of type float')
        if sep <= 0:
            raise ValueError('Argument must be greater than zero')
        self._copper_sep = pcbnew.FromMM(sep)

    @property
    def mill_sep(self):
        return self._mill_sep

    @mill_sep.setter
    def mill_sep(self, sep):
        if not isinstance(sep, int):
            raise TypeError('Argument must be of type int')
        if sep <= 0:
            raise ValueError('Argument must be greater than zero')
        self._mill_sep = sep

    @property
    def mill_sep_mm(self):
        return pcbnew.ToMM(self._mill_sep)

    @mill_sep_mm.setter
    def mill_sep_mm(self, sep):
        if not isinstance(sep, float):
            raise TypeError('Argument must be of type float')
        if sep <= 0:
            raise ValueError('Argument must be greater than zero')
        self._mill_sep = pcbnew.FromMM(sep)

    @property
    def mill_tol(self):
        return self._mill_tol

    @mill_tol.setter
    def mill_tol(self, tol):
        if not isinstance(tol, int):
            raise TypeError('Argument must be of type int')
        if tol <= 0:
            raise ValueError('Argument must be greater than zero')
        self._mill_tol = tol

    @property
    def mill_tol_mm(self):
        return pcbnew.ToMM(self._mill_tol)

    @mill_tol_mm.setter
    def mill_tol_mm(self, tol):
        if not isinstance(tol, float):
            raise TypeError('Argument must be of type float')
        if tol <= 0:
            raise ValueError('Argument must be greater than zero')
        self._mill_tol = pcbnew.FromMM(tol)

    @property
    def track_width(self):
        return self._track_width

    @track_width.setter
    def track_width(self, width):
        if not isinstance(width, int):
            raise TypeError('Argument must be of type int')
        if width <= 0:
            raise ValueError('Argument must be greater than zero')
        self._track_width = width

    @property
    def track_width_mm(self):
        return pcbnew.ToMM(self._track_width)

    @track_width_mm.setter
    def track_width_mm(self, width):
        if not isinstance(width, float):
            raise TypeError('Argument must be of type float')
        if width <= 0:
            raise ValueError('Argument must be greater than zero')
        self._track_width = pcbnew.FromMM(width)

    @property
    def via_drill(self):
        return self._via_drill

    @via_drill.setter
    def via_drill(self, drill):
        if not isinstance(drill, int):
            raise TypeError('Argument must be of type int')
        if drill <= 0:
            raise ValueError('Argument must be greater than zero')
        self._via_drill = drill

    @property
    def via_drill_mm(self):
        return pcbnew.ToMM(self._via_drill)

    @via_drill_mm.setter
    def via_drill_mm(self, drill):
        if not isinstance(drill, float):
            raise TypeError('Argument must be of type float')
        if drill <= 0:
            raise ValueError('Argument must be greater than zero')
        self._via_drill = pcbnew.FromMM(drill)

    @property
    def via_width(self):
        return self._via_width

    @via_width.setter
    def via_width(self, width):
        if not isinstance(width, int):
            raise TypeError('Argument must be of type int')
        if width <= 0:
            raise ValueError('Argument must be greater than zero')
        self._via_width = width

    @property
    def via_width_mm(self):
        return pcbnew.ToMM(self._via_width)

    @via_width_mm.setter
    def via_width_mm(self, width):
        if not isinstance(width, float):
            raise TypeError('Argument must be of type float')
        if width <= 0:
            raise ValueError('Argument must be greater than zero')
        self._via_width = pcbnew.FromMM(width)


class Coil():
    '''Coil geometry
    '''

    def __init__(self, board, design_parameter = DesignParameter, layer_count: int = 2, winding_count: int = 2):
        
        self.design_parameter = design_parameter

        self._center_tap = None

        self.layer_count = layer_count
        self.winding_count = winding_count

    def __str__(self):
        return (
            f'Coil has {self.layer_count} layers and a total of '
            f'{self.winding_count} windings, '
            f'thus {self.layer_winding_count} windings per layer.'
            f'\n{self.design_parameter}'
        )

    def set_layer_count(self, new_layer_count):
        #Set layer count to new input
        self.layer_count = new_layer_count

    def set_winding_count(self, new_winding_count):
        #Set winding count to new input
        self.winding_count = new_winding_count

    #Error messages for wrong inputs
    @property
    def center_tap(self):
        return self._center_tap

    @property
    def layer_count(self):
        return self._layer_count

    @layer_count.setter
    def layer_count(self, count):
        if not isinstance(count, int):
            raise TypeError('Argument must be of type int')
        if count < 1:
            raise ValueError('Argument must be greater than zero')
        self._layer_count = count

    @property
    def layer_winding_count(self):
        return self._layer_winding_count

    @property
    def winding_count(self):
        return self._winding_count

    @winding_count.setter
    def winding_count(self, count):
        if not isinstance(count, int):
            raise TypeError('Argument must be of type int')
        if count < 1:
            raise ValueError('Argument must be greater than zero')
        if count < self.layer_count:
            raise ValueError(
                'Coil must have at least as many windings as layers')
        self._winding_count = count
        self._layer_winding_count = self.winding_count / self.layer_count


class Transformer():
    def __init__(self, board, module, core_type, core_size):
        self._board = board
        self._module = module

        self._core_type = core_type
        self._core_size = core_size

        if core_type == 'ELT':
            self._core = ELT(self.board, self.module,
                             core_type, core_size, None)
        elif core_type == 'EL':
            self._core = ELT(self.board, self.module,
                             core_type, core_size, None)
        else:
            raise NotImplementedError(
                  f'Core type {core_type} not implemented')

        #self._design_parameter = DesignParameter(board = board)
        self._primary = []
        self._secondary = []

        self._pcb_layer_count = 4
        self._pcb_config =[]
        
        self._four_layer_pcb =[util.Layer.F_Cu, util.Layer.In1_Cu, util.Layer.In2_Cu, util.Layer.B_Cu]
        self._six_layer_pcb = [util.Layer.F_Cu, util.Layer.In1_Cu, util.Layer.In2_Cu, util.Layer.In3_Cu, util.Layer.In4_Cu, util.Layer.B_Cu]
        self._twelve_layer_pcb = [util.Layer.F_Cu, util.Layer.In1_Cu, util.Layer.In2_Cu, util.Layer.In3_Cu, util.Layer.In4_Cu, util.Layer.In5_Cu, 
                                    util.Layer.In6_Cu, util.Layer.In7_Cu, util.Layer.In8_Cu, util.Layer.In9_Cu, util.Layer.In10_Cu, util.Layer.B_Cu]

    def __str__(self):
        bullet = '\n   * '
        print(f'{bullet.join(f"{p.layer_count}" for p in self._primary)}')

        return (
            f'Transformer'
            f'\n - Core {self.core.core_type}{self.core.core_size}'
            f'\n - {len(self._primary)} primary coil{"" if (len(self._primary) == 1) else "s"}:'

        )

    def add_primary(self, coil: Coil):
        if isinstance(coil, Coil):
            self._primary.append(coil)
        else:
            raise TypeError('Argument not of type Coil')

    def add_secondary(self, coil: Coil):
        if isinstance(coil, Coil):
            self._secondary.append(coil)
        else:
            raise TypeError('Argument not of type Coil')

    #Set new parameters
    def set_core_type(self, new_core_type):
        self._core_type = new_core_type

    def set_core_size(self, new_core_size):
        self._core_size = new_core_size

    def set_pcb_config(self, new_pcb_config):
        self._pcb_config = new_pcb_config

    def set_pcb_config_to_net_code(self):
        #Sets the chosen pcb configuration to configuration in netcode
        self.pcb_config_netcode = []
        for x in range(0, len(self.pcb_config)):
            if self.pcb_config[x] == '/PRIMARY':
                self.pcb_config_netcode.append(1)
            else:
                self.pcb_config_netcode.append(3)
        
        return self.pcb_config_netcode
    
    def get_primary_layer_count(self):
        self.primary_layer_count = 0
        for x in range(0, len(self.pcb_config)):
            if self.pcb_config[x] == '/PRIMARY':
                self.primary_layer_count = self.primary_layer_count + 1

        return self.primary_layer_count
    
    def get_layer_properties(self):
        #Returns list of according properties of util.Layer class
        if self._pcb_layer_count == 4:
            self.layer_properties = self._four_layer_pcb

        elif self._pcb_layer_count == 6:
            self.layer_properties = self._six_layer_pcb

        else:
            self.layer_properties = self._twelve_layer_pcb

        return self.layer_properties

    @property
    def core(self):
        return self._core

    @property
    def board(self):
        return self._board

    @board.setter
    def board(self, board):
        self._board = board

    @property
    def module(self):
        return self._module

    @module.setter
    def module(self, module):
        self._module = module
    
    @property
    def pcb_config(self):
        return self._pcb_config

    @property
    def core_type(self):
        return self._core_type

    @property
    def core_size(self):
        return self._core_size
