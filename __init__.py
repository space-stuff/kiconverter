# -*- coding: utf-8 -*-

import sys
import traceback

try:
    print("converter: try to register")
    # test for wxpython
    import wx
    # Note the relative import!
    from .action_converter import Converter
    # Instantiate and register to Pcbnew
    Converter().register()
    print("converter: registered")
except Exception as e:
    import os
    plugin_dir = os.path.dirname(os.path.realpath(__file__))
    log_file = os.path.join(plugin_dir, 'Converter_error.log')
    traceback.print_exc(file=sys.stdout)
    with open(log_file, 'w') as f:
        f.write(repr(e))
    from .no_wxpython import NoWxpython as Converter
    Converter().register()
