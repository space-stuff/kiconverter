#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#Create logging file util.log
import logging
import os
import copy
import math
import pcbnew

from enum import IntEnum


class Layer(IntEnum):
    F_Cu = 0
    In1_Cu = 1
    In2_Cu = 2
    In3_Cu = 3
    In4_Cu = 4
    In5_Cu = 5
    In6_Cu = 6
    In7_Cu = 7
    In8_Cu = 8
    In9_Cu = 9
    In10_Cu = 10
    In11_Cu = 11
    In12_Cu = 12
    In13_Cu = 13
    In14_Cu = 14
    In15_Cu = 15
    In16_Cu = 16
    In17_Cu = 17
    In18_Cu = 18
    In19_Cu = 19
    In20_Cu = 20
    In21_Cu = 21
    In22_Cu = 22
    In23_Cu = 23
    In24_Cu = 24
    In25_Cu = 25
    In26_Cu = 26
    In27_Cu = 27
    In28_Cu = 28
    In29_Cu = 29
    In30_Cu = 30
    B_Cu = 31
    B_Adhes = 32
    F_Adhes = 33
    B_Paste = 34
    F_Paste = 35
    B_SilkS = 36
    F_SilkS = 37
    B_Mask = 38
    F_Mask = 39
    Dwgs_User = 40
    Cmts_User = 41
    Eco1_User = 42
    Eco2_User = 43
    Edge_Cuts = 44
    Margin = 45
    B_CrtYd = 46
    F_CrtYd = 47
    B_Fab = 48
    F_Fab = 49


class STROKE_T(IntEnum):
    S_SEGMENT = 0
    S_RECT = 1
    S_ARC = 2
    S_CIRCLE = 3
    S_POLYGON = 4
    S_CURVE = 5


class EDA_TEXT_HJUSTIFY_T(IntEnum):
    GR_TEXT_HJUSTIFY_LEFT = -1
    GR_TEXT_HJUSTIFY_CENTER = 0
    GR_TEXT_HJUSTIFY_RIGHT = 1


class EDA_TEXT_VJUSTIFY_T(IntEnum):
    GR_TEXT_VJUSTIFY_TOP = -1
    GR_TEXT_VJUSTIFY_CENTER = 0
    GR_TEXT_VJUSTIFY_BOTTOM = 1


class VIA_TYPE(IntEnum):
    VIA_NOT_DEFINED = 0
    VIA_MICROVIA = 1
    VIA_BLIND_BURIED = 2
    VIA_THROUGH = 3


def to_point(instance):
    """Convert input to pcbnew wxPoint
    """
    if isinstance(instance, pcbnew.wxPoint):
        point = instance

    elif isinstance(instance, pcbnew.VECTOR2I):
        point = pcbnew.wxPoint(instance.x, instance.y)

    elif isinstance(instance, pcbnew.PCB_VIA):
        point = instance.GetPosition()

    elif isinstance(instance, list):
        point = []

        for item in instance:
            point.append(to_point(item))

    elif isinstance(instance, tuple):
        if len(tuple) == 2:
            point = pcbnew.wxPoint(instance[0], instance[1])

        else:
            raise TypeError("input tuple has more or less than two elements")

    else:
        raise TypeError("input parameter unknown for conversion to pcbnew.wxPoint")

    return point


def to_vector(instance):
    """Convert input to pcbnew VECTOR2I
    """
    if isinstance(instance, pcbnew.VECTOR2I):
        # do nothing for vectors
        vector = instance

    elif isinstance(instance, pcbnew.wxPoint):
        # convert wxPoint directly to vector
        vector = pcbnew.VECTOR2I(instance)

    elif isinstance(instance, pcbnew.PCB_VIA):
        # convert via position
        vector = pcbnew.VECTOR2I(instance.GetPosition())

    elif isinstance(instance, list):
        # handle lists
        vector = []

        for item in instance:
            vector.append(to_vector(item))

    elif isinstance(instance, tuple):
        # handle tuples
        if len(tuple) == 2:
            vector = pcbnew.VECTOR2I(instance[0], instance[1])

        else:
            raise TypeError("input tuple has more or less than two elements")

    else:
        raise TypeError("input parameter unknown for conversion to pcbnew.VECTOR2I")

    return vector


def to_module_frame(module: pcbnew.FOOTPRINT, instance):
    """Return a copy of the instance transformed to the module coordinate frame
    """
    offset = module.GetPosition()
    angle = module.GetOrientationRadians()

    if isinstance(instance, pcbnew.wxPoint):
        point = pcbnew.wxPoint(instance.x - offset.x, instance.y - offset.y)
        items = to_point(to_vector(point).Rotate(angle))
    elif isinstance(instance, list):
        items = []

        for item in instance:
            items.append(to_module_frame(module, item))
    else:
        raise TypeError("input parameter unknown for transformation to module frame")
    return items


def to_global_frame(module: pcbnew.FOOTPRINT, instance):
    """Return a copy of the instance transformed to the global coordinate frame
    """
    offset = module.GetPosition()
    angle = module.GetOrientationRadians()

    if isinstance(instance, pcbnew.wxPoint):
        items = to_point(to_vector(instance).Rotate(-angle))
        items = pcbnew.wxPoint(items.x + offset.x, items.y + offset.y)
    elif isinstance(instance, list):
        items = []

        for item in instance:
            items.append(to_global_frame(module, item))
    else:
        raise TypeError("input parameter unknown for transformation to global frame")
    return items


def perpendicular_scale(
    p0=pcbnew.VECTOR2I(pcbnew.wxPointMM(0, 0)),
    p1=pcbnew.VECTOR2I(pcbnew.wxPointMM(0, 5)),
    length=pcbnew.FromMM(1),
):
    v = to_vector(p1) - to_vector(p0)
    w = v.Perpendicular().Resize(length)
    return p1 + to_point(w)


def points_on_arc(
    center=pcbnew.VECTOR2I(0, 0), start=pcbnew.VECTOR2I(1, 0), angle=math.pi, count=18
):
    """Calculate positions of points on an arc defined by its center, a start
    point, an angle, and the count of segments

    The points are defined in such a way that if they were connected by
    straight line segments, each segment would tangent the virtual circle
    defined by center and start point exactly in the middle of the segment.
    Only the first and last segment differ from this because there the start
    or end point, respectively, are tangent to the circle.
    """
    center = to_vector(center)  # center vector
    start = to_vector(start)  # start vector
    delta = angle / count  # angle between points
    v = start - center  # vector from center to start
    points = [start.getWxPoint()]  # start point is on circle
    r = v.EuclideanNorm()  # arc radius is length of v
    s = int(r * math.tan(delta / 2))  # length of first/last segment
    w = v + v.Perpendicular().Resize(s)  # vector of first segment

    for i in range(count):
        # append points between start and end of arc
        points.append((center + w.Rotate(i * delta)).getWxPoint())

    points.append((center + v.Rotate(angle)).getWxPoint())  # append end point

    # TODO: how to deal with logging?
    # print('points_on_arc: center at ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(center.x), pcbnew.ToMM(center.y)))
    # print('points_on_arc: start at ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(start.x), pcbnew.ToMM(start.y)))
    # print('points_on_arc: δ = {: 10.6f}°'.format(math.degrees(delta)))
    # print('points_on_arc: v = ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(v.x), pcbnew.ToMM(v.y)))
    # print('points_on_arc: r = {: 10.6f} mm'.format(
    #     pcbnew.ToMM(r)))
    # print('points_on_arc: s = {: 10.6f} mm'.format(
    #     pcbnew.ToMM(s)))
    # print('points_on_arc: w = ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(w.x), pcbnew.ToMM(w.y)))

    # TODO: should output better be some kind of pcbnew.VECTOR2I array thingy?
    #       + What would be positive about this?
    return points


def mirror_about_y_axis(point, mirror=0):
    """
    Mirror the point about the horizontal axis given in mm
    """
    y = pcbnew.FromMM(mirror) - (point.y - pcbnew.FromMM(mirror))
    mirrored_point = pcbnew.wxPoint(point.x, y)

    # print(point)
    # print(mirror)
    # print(mirrored_point)

    return mirrored_point


def mirror_x_component(point, mirror):
    """
    Mirror the x component of a point w.r.t. the x component of the mirror point.
    """
    x = mirror.x - (point.x - mirror.x)
    mirrored_point = pcbnew.wxPoint(x, point.y)

    # print(point)
    # print(mirror)
    # print(mirrored_point)

    return mirrored_point


def mirror_y_component(point, mirror):
    """
    Mirror the y component of a point w.r.t. the y component of the mirror point.
    """
    y = mirror.y - (point.y - mirror.y)
    mirrored_point = pcbnew.wxPoint(point.x, y)

    # print(point)
    # print(mirror)
    # print(mirrored_point)

    return mirrored_point


def draw_segment(
    board,
    start,
    end,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a straight line segment
    """
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(STROKE_T.S_SEGMENT)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetStart(to_point(start))
    segment.SetEnd(to_point(end))

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(rotation_center, rotation_angle)

    # move segment by a given vector
    segment.Move(center)

    if debug:
        logging.info("util.draw_segment: add line segment")
        print("util.draw_segment: add line segment")
    board.Add(segment)


def draw_segments(
    board,
    points,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw segments
    """
    for i in range(len(points) - 1):
        draw_segment(
            board,
            points[i],
            points[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def draw_arc(
    board,
    arc_center,
    arc_start,
    arc_angle,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw an arc segment
    """
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(STROKE_T.S_ARC)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetCenter(arc_center)
    segment.SetStart(arc_start)
    segment.SetArcAngleAndEnd(arc_angle)

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(rotation_center, rotation_angle)

    # move segment by a given vector
    segment.Move(center)

    if debug:
        logging.info("util.draw_arc: add arc segment")
        print("util.draw_arc: add arc segment")
    board.Add(segment)


def draw_circle(
    board,
    center=pcbnew.wxPointMM(0, 0),
    radius=10,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    offset=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a circle line segment
    """
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(STROKE_T.S_CIRCLE)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetCenter(to_point(center))
    segment.SetStart(to_point(center) + pcbnew.wxPointMM(radius, 0))

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(rotation_center, rotation_angle)

    # move segment by a given vector
    segment.Move(offset)

    if debug:
        logging.info("util.draw_circle: add circle segment")
        print("util.draw_circle: add circle segment")
    board.Add(segment)


def draw_stadium(
    board,
    length,
    width,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    offset=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a stadium shape
    """
    # TODO: adapt this similar to draw_rounded_rectangle
    
    if debug:
        logging.info("util.draw_stadium: draw stadium")
        print("util.draw_stadium: draw stadium")

    length_short = length - width

    p = []

    p.append(pcbnew.wxPointMM(length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length_short / 2, -width / 2) + offset)

    c = []

    c.append(pcbnew.wxPointMM(length_short / 2, 0) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, 0) + offset)

    for i in range(0, len(p), 2):
        draw_segment(
            board,
            p[i],
            p[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )

    for i in range(0, len(c)):
        draw_arc(
            board,
            c[i],
            p[2 * i - 1],
            1800,
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def draw_rounded_rectangle(
    board,
    length,
    width,
    radius,
    layer=Layer.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    offset=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a rounded rectangle shape
    """

    if debug:
        logging.info("util.draw_rounded_rectangle: draw rounded rectangle")
        print("util.draw_rounded_rectangle: draw rounded rectangle")

    length_short = length - 2 * radius
    width_short = width - 2 * radius

    p = []

    p.append(pcbnew.wxPointMM(length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length / 2, width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(-length / 2, -width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length / 2, -width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(length / 2, width_short / 2) + offset)

    c = []

    c.append(pcbnew.wxPointMM(length_short / 2, width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, -width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(length_short / 2, -width_short / 2) + offset)

    for i in range(0, len(p), 2):
        draw_segment(
            board,
            p[i],
            p[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )

    for i in range(0, len(c)):
        draw_arc(
            board,
            c[i],
            p[2 * i - 1],
            900,
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def add_pcb_arc(board, start, center, end, width, layer, netcode, reverse=False):
    """
    Add an arced PCB geometry to the board by providing the center, start, and end
    point of the arc.
    """
    arc = pcbnew.PCB_ARC(board)

    start = to_point(start)
    center = to_point(center)
    end = to_point(end)

    # mid is the vector from center to start rotated about the center by half the angle between the two vectors
    center_vector = to_vector(center)
    start_vector = to_vector(start - center)
    end_vector = to_vector(end - center)

    angle = end_vector.Angle() - start_vector.Angle()
    if angle < 0:
        angle += 2.0 * math.pi

    mid_angle = angle / 2.0

    if not reverse:
        mid = to_point(center_vector + start_vector.Rotate(mid_angle))
    else:
        mid = to_point(center_vector - start_vector.Rotate(mid_angle))

    # print(start_vector.Angle())
    # print(center)
    # print(end_vector.Angle())

    arc.SetStart(start)
    arc.SetEnd(end)
    arc.SetMid(mid)
    arc.SetWidth(pcbnew.FromMM(width))
    arc.SetLayer(layer)
    arc.SetNetCode(netcode)

    board.Add(arc)


def add_track(
    board,
    start,
    end,
    width=0.15,
    layer=Layer.F_Cu,
    netcode=0,
    vector=pcbnew.wxPointMM(0, 0),
    angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add track
    """
    if not isinstance(start, pcbnew.wxPoint):
        raise TypeError("Start point must be of type 'pcbnew.wxPoint'")

    if not isinstance(end, pcbnew.wxPoint):
        raise TypeError("End point must be of type 'pcbnew.wxPoint'")

    track = pcbnew.PCB_TRACK(board)

    track.SetStart(start)
    track.SetEnd(end)
    track.SetWidth(pcbnew.FromMM(width))
    track.SetNetCode(netcode)
    track.SetLayer(layer)
    track.Rotate(rotation_center, angle)
    track.Move(vector)

    board.Add(track)

    if debug:
        s = "util.add_track:  from ({: 10.6f},{: 10.6f})" + " to ({: 10.6f},{: 10.6f})"
        logging.info(
            s.format(
                pcbnew.ToMM(start.x),
                pcbnew.ToMM(start.y),
                pcbnew.ToMM(end.x),
                pcbnew.ToMM(end.y),
            )
        )
        print(
            s.format(
                pcbnew.ToMM(start.x),
                pcbnew.ToMM(start.y),
                pcbnew.ToMM(end.x),
                pcbnew.ToMM(end.y),
            )
        )
    logging.info("add_track")
    return track


def add_tracks(
    board,
    points,
    width=pcbnew.FromMM(0.15),
    layer=Layer.F_Cu,
    netcode=0,
    vector=pcbnew.wxPointMM(0, 0),
    angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add tracks
    """
    if debug:
        logging.info("util.add_tracks: add tracks")
        print("util.add_tracks: add tracks")

    tracks = []

    for i in range(len(points) - 1):
        tracks.append(
            add_track(
                board,
                points[i],
                points[i + 1],
                width,
                layer,
                netcode,
                vector,
                angle,
                rotation_center,
                debug,
            )
        )
    
    return tracks


def add_arced_track(
    board,
    center=pcbnew.wxPointMM(0, 0),
    start=pcbnew.wxPointMM(1, 0),
    angle=180,
    elements=18,
    track_width=0.15,
    layer=Layer.F_Cu,
    netcode=0,
    offset=pcbnew.wxPointMM(0, 0),
    orientation=0,
    debug: bool = False,
):
    """Add arced track
    """
    ##compare line 560
    if debug:
        logging.info("util.add_Arced_track: add arced track")
        print("util.add_Arced_track: add arced track")
    
    #arced_track = pcbnew.PCB_ARC(board)

    #start = to_point(start)
    #center = to_point(center)
    #end = to_point(end)

    # mid is the vector from center to start rotated about the center by half the angle between the two vectors
    #center_vector = to_vector(center)
    #start_vector = to_vector(start - center)
    #end_vector = to_vector(end - center)

    #angle = end_vector.Angle() - start_vector.Angle()
    #if angle < 0:
    #    angle += 2.0 * math.pi

    #mid_angle = angle / 2.0

    #if not reverse:
    #    mid = to_point(center_vector + start_vector.Rotate(mid_angle))
    #else:
    #    mid = to_point(center_vector - start_vector.Rotate(mid_angle))

    #arced_track.SetStart(start)
    #arced_track.SetMid(mid)
    #arced_track.SetEnd(end)
    #arced_track.SetWidth(pcbnew.FromMM(track_width))
    #arced_track.SetNetCode(netcode)
    #arced_track.SetLayer(layer)

    #board.Add(arced_track)

    points = points_on_arc(center, start, math.radians(angle), elements)
    arc = []
    for i in range(1, len(points)):
        arc.append(
            add_track(
                board,
                points[i - 1],
                points[i],
                pcbnew.FromMM(track_width),
                layer,
                netcode,
                offset,
                orientation,
                debug,
            )
        )
    #TODO Fehler vielleicht hier
    logging.info("add_arced_track")
    return arc
    #return arced_track


def add_via(
    board,
    position=pcbnew.wxPointMM(0, 0),
    width=0.4,
    drill=0.2,
    top_layer=Layer.F_Cu,
    bottom_layer=Layer.B_Cu,
    via_type=pcbnew.VIATYPE_THROUGH,
    netcode=1,
    offset=pcbnew.wxPoint(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPoint(0, 0),
    debug=False,
):
    """Add via
    """
    via = pcbnew.PCB_VIA(board)

    via.SetPosition(to_point(position))
    via.SetViaType(via_type)
    via.SetTopLayer(top_layer)
    via.SetBottomLayer(bottom_layer)
    via.Rotate(rotation_center, rotation_angle)
    via.Move(offset)
    via.SetNetCode(netcode)
    via.SetWidth(pcbnew.FromMM(width))
    via.SetDrill(pcbnew.FromMM(drill))

    board.Add(via)

    if debug:
        logging.info(
            "util.add_via: add via at ({: 10.6f},{: 10.6f})".format(
                pcbnew.ToMM(position.x), pcbnew.ToMM(position.y)
            )       
        )
        print(
            "util.add_via: add via at ({: 10.6f},{: 10.6f})".format(
                pcbnew.ToMM(position.x), pcbnew.ToMM(position.y)
            )
        )
    
    return via


def add_vias_on_arc(
    board,
    center=pcbnew.wxPointMM(0, 0),
    start=pcbnew.wxPointMM(10, 0),
    distance=0.55,
    count=3,
    width=pcbnew.FromMM(0.4),
    drill=pcbnew.FromMM(0.2),
    top_layer=Layer.F_Cu,
    bottom_layer=Layer.B_Cu,
    via_type=pcbnew.VIATYPE_THROUGH,
    netcode=1,
    offset=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add vias on arc
    """
    vector = to_vector(start - center)
    radius = pcbnew.ToMM(vector.EuclideanNorm())
    via_angle = 2 * math.asin(distance / (2 * radius))

    vias = []
    if debug:
        logging.info("util.add_vias_on_arc: center = {}".format(center))
        logging.info("util.add_vias_on_arc: start  = {}".format(start))
        logging.info("util.add_vias_on_arc: vector = {}".format(vector.getWxPoint()))
        logging.info("util.add_vias_on_arc: radius = {}".format(radius))
        logging.info("util.add_vias_on_arc: distance = {}".format(distance))
        logging.info("util.add_vias_on_arc: angle = {} deg".format(math.degrees(via_angle)))
        logging.info("util.add_vias_on_arc: add {} vias".format(count))        
        print("util.add_vias_on_arc: center = {}".format(center))
        print("util.add_vias_on_arc: start  = {}".format(start))
        print("util.add_vias_on_arc: vector = {}".format(vector.getWxPoint()))
        print("util.add_vias_on_arc: radius = {}".format(radius))
        print("util.add_vias_on_arc: distance = {}".format(distance))
        print("util.add_vias_on_arc: angle = {} deg".format(math.degrees(via_angle)))
        print("util.add_vias_on_arc: add {} vias".format(count))
    for i in range(count):
        position = center + vector.Rotate(i * via_angle).getWxPoint()
        vias.append(
            add_via(
                board,
                position,
                width,
                drill,
                top_layer,
                bottom_layer,
                via_type,
                netcode,
                offset,
                rotation_angle,
                rotation_center,
                debug,
            )
        )
    
    return vias


def get_nearest_segment(target, points):
    target = to_vector(target)
    points = to_vector(points)

    distance = []
    for point in points:
        distance.append((point - target).EuclideanNorm())

    idx = sorted(range(len(distance)), key=lambda k: distance[k])[0:2]
    
    return to_point([points[idx[0]], points[idx[1]]]), min(idx)


def split_at_nearest_segment(target, points, how="y", keep_before=True):
    """Split segment at segment nearest to tearget point
    """
    segment, end_index = get_nearest_segment(target, points)

    # point on split segment
    if how == "y":
        m = (segment[1].y - segment[0].y) / (segment[1].x - segment[0].x)
        b = -segment[1].x * m + segment[1].y
        split_point = pcbnew.wxPoint((target.y - b) / m, target.y)
    elif how == "x":
        raise NotImplementedError()
    elif how == "perpendicular":
        raise NotImplementedError()
    else:
        raise ValueError()

    # split segments at selected point
    out_points = []
    if keep_before:
        out_points.extend(points[0:end_index])
        out_points.append(split_point)
    else:
        out_points.append(split_point)
        out_points.extend(points[end_index:])
    #TODO vllt Fehler hier
    logging.info("split_at_nearest_segment")
    return out_points


def project_orthogonally(a, b):
    a = to_vector(a)
    b = to_vector(b)

    s = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y) * a.EuclideanNorm()

    c = a.Resize(int(s))
    #TODO vllt Fehler hier
    logging.info("project_orthogonally")
    return c


def line_intersection(a, b):
    """Find the intersection p of lines a, b
    """
    x1, y1 = a[0].x, a[0].y
    x2, y2 = a[1].x, a[1].y
    x3, y3 = b[0].x, b[0].y
    x4, y4 = b[1].x, b[1].y

    d12 = x1 * y2 - y1 * x2
    d34 = x3 * y4 - y3 * x4

    den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    
    return pcbnew.wxPoint(
        (d12 * (x3 - x4) - (x1 - x2) * d34) / den,
        (d12 * (y3 - y4) - (y1 - y2) * d34) / den,
    )

def load_footprint(board, footprint, position):

    LIBPATH = os.getcwd() + "/../../test/Transformer_Flat.pretty"

    footprint_load = pcbnew.FootprintLoad(LIBPATH, footprint)
    footprint_load.SetPosition(position)

    board.Add(footprint_load)

    return footprint_load