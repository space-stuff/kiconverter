#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 23:54:23 2020

@author: seba
"""

import pcbnew

import json

import numpy as np

from enum import IntEnum

# These classes miss in the exported interface

class Layer(IntEnum):
    B_Adhes = 32
    F_Adhes = 33
    B_Paste = 34
    F_Paste = 35
    B_SilkS = 36
    F_SilkS = 37
    B_Mask = 38
    F_Mask = 39
    Dwgs_User = 40
    Cmts_User = 41
    Eco1_User = 42
    Eco2_User = 43
    Edge_Cuts = 44
    Margin = 45
    B_CrtYd = 46
    F_CrtYd = 47
    B_Fab = 48
    F_Fab = 49

class STROKE_T(IntEnum):
    S_SEGMENT = 0
    S_RECT = 1
    S_ARC = 2
    S_CIRCLE = 3
    S_POLYGON = 4
    S_CURVE = 5

class EDA_TEXT_HJUSTIFY_T(IntEnum):
    GR_TEXT_HJUSTIFY_LEFT   = -1
    GR_TEXT_HJUSTIFY_CENTER = 0
    GR_TEXT_HJUSTIFY_RIGHT  = 1

class EDA_TEXT_VJUSTIFY_T(IntEnum):
    GR_TEXT_VJUSTIFY_TOP    = -1
    GR_TEXT_VJUSTIFY_CENTER = 0
    GR_TEXT_VJUSTIFY_BOTTOM = 1

print("go!")

'''
Try to access the current board or create a new one
'''
board = pcbnew.GetBoard()

if board is None:
    board = pcbnew.BOARD()

with open("dimensions.json", "r") as filename:
    cores = json.load(filename)

coreType = 'EL'
coreSize = '11X4'

core = cores[coreType][coreSize]

'''
Draw the edge
'''
x = 5.08
y = 5.08

def drawELCore(board, coreType, coreSize, dimensions, origin=pcbnew.wxPointMM(0, 0), tol=0.1):
    print("Add core {0}{1}".format(coreType, coreSize))
    a  = dimensions['a']
    c  = dimensions['c']
    e  = dimensions['e']
    f1 = dimensions['f1']
    f2 = dimensions['f2']
    r2 = dimensions['r2']

    # Center slot (slotted hole)
    print("Add slotted hole")
    # TODO: create function to draw slot: length, width, origin, rotation
    x = (f1[0] + f1[1]) / 2 + tol
    y = (f2[0] - f1[0]) / 2

    startPoint = pcbnew.wxPointMM(x, -y) + origin
    endPoint   = pcbnew.wxPointMM(x,  y) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM(0, y) + origin
    startPoint  = endPoint
    angle       = 180.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint = pcbnew.wxPointMM(-x,  y) + origin
    endPoint   = pcbnew.wxPointMM(-x, -y) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM(0, -y) + origin
    startPoint  = endPoint
    angle       = 180.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    # Right slot (rounded rectangle)
    print("Add rounded rectangle")
    # TODO: create function to draw rounded rectangle: length, width, radius, origin rotation
    x1 = (a[0] + a[1]) / 2 + tol
    x2 =  a[0]         / 2 - r2[0]
    x3 =  e[0]         / 2 + r2[0]
    x4 = (e[0] - e[1]) / 2 - tol
    y1 =  c[0]         / 2 - r2[0]
    y2 = (c[0] + c[1]) / 2 + tol

    startPoint  = pcbnew.wxPointMM( x1, -y1) + origin
    endPoint    = pcbnew.wxPointMM( x1,  y1) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x2,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x2,  y2) + origin
    endPoint    = pcbnew.wxPointMM( x3,  y2) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x3,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x4,  y1) + origin
    endPoint    = pcbnew.wxPointMM( x4, -y1) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x3, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x3, -y2) + origin
    endPoint    = pcbnew.wxPointMM( x2, -y2) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x2, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    # Right slot (rounded rectangle)
    print("Add rounded rectangle")
    # TODO: create function to draw rounded rectangle: length, width, radius, origin rotation
    x4 = -((a[0] + a[1]) / 2 + tol)
    x3 = -( a[0]         / 2 - r2[0])
    x2 = -( e[0]         / 2 + r2[0])
    x1 = -((e[0] - e[1]) / 2 - tol)
    y1 =  ( c[0]         / 2 - r2[0])
    y2 =  ((c[0] + c[1]) / 2 + tol)

    startPoint  = pcbnew.wxPointMM( x1, -y1) + origin
    endPoint    = pcbnew.wxPointMM( x1,  y1) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x2,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x2,  y2) + origin
    endPoint    = pcbnew.wxPointMM( x3,  y2) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x3,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x4,  y1) + origin
    endPoint    = pcbnew.wxPointMM( x4, -y1) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x3, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    startPoint  = pcbnew.wxPointMM( x3, -y2) + origin
    endPoint    = pcbnew.wxPointMM( x2, -y2) + origin

    drawSegment(board, startPoint, endPoint)

    centerPoint = pcbnew.wxPointMM( x2, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0)

    # Core contour: Center slot (slotted hole)
    print("Add slotted hole")
    # TODO: create function to draw slot: length, width, origin, rotation
    x =  f1[0]          / 2
    y = (f2[0] - f1[0]) / 2

    startPoint = pcbnew.wxPointMM(x, -y) + origin
    endPoint   = pcbnew.wxPointMM(x,  y) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM(0, y) + origin
    startPoint  = endPoint
    angle       = 180.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint = pcbnew.wxPointMM(-x,  y) + origin
    endPoint   = pcbnew.wxPointMM(-x, -y) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM(0, -y) + origin
    startPoint  = endPoint
    angle       = 180.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    # Core contour (rounded rectangle)
    print("Add rounded rectangle")
    # TODO: create function to draw rounded rectangle: length, width, radius, origin rotation
    x1 = a[0] / 2
    x2 = a[0] / 2 - r2[0]
    x3 = e[0] / 2 + r2[0]
    x4 = e[0] / 2
    y1 = c[0] / 2 - r2[0]
    y2 = c[0] / 2

    startPoint  = pcbnew.wxPointMM( x1, -y1) + origin
    endPoint    = pcbnew.wxPointMM( x1,  y1) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x2,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x2,  y2) + origin
    endPoint    = pcbnew.wxPointMM( x3,  y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x3,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x4,  y1) + origin
    endPoint    = pcbnew.wxPointMM( x4, -y1) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x3, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x3, -y2) + origin
    endPoint    = pcbnew.wxPointMM( x2, -y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x2, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    # Core contour (rounded rectangle)
    print("Add rounded rectangle")
    # TODO: create function to draw rounded rectangle: length, width, radius, origin rotation
    x4 = -a[0] / 2
    x3 = -a[0] / 2 + r2[0]
    x2 = -e[0] / 2 - r2[0]
    x1 = -e[0] / 2
    y1 =  c[0] / 2 - r2[0]
    y2 =  c[0] / 2

    startPoint  = pcbnew.wxPointMM( x1, -y1) + origin
    endPoint    = pcbnew.wxPointMM( x1,  y1) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x2,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x2,  y2) + origin
    endPoint    = pcbnew.wxPointMM( x3,  y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x3,  y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x4,  y1) + origin
    endPoint    = pcbnew.wxPointMM( x4, -y1) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x3, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM( x3, -y2) + origin
    endPoint    = pcbnew.wxPointMM( x2, -y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    centerPoint = pcbnew.wxPointMM( x2, -y1) + origin
    startPoint  = endPoint
    angle       =  90.0

    drawArc(board, centerPoint, startPoint, angle * 10.0, layer=Layer.Dwgs_User)

    # Draw segments between exterior slots
    startPoint  = pcbnew.wxPointMM( x2,  y2) + origin
    endPoint    = pcbnew.wxPointMM(-x2,  y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

    startPoint  = pcbnew.wxPointMM(-x2, -y2) + origin
    endPoint    = pcbnew.wxPointMM( x2, -y2) + origin

    drawSegment(board, startPoint, endPoint, layer=Layer.Dwgs_User)

def drawArc(board, arcCenterPoint, arcStartPoint, arcAngle, layer=Layer.Edge_Cuts):
    '''
    Draw a straight line segment
    '''
    segment = pcbnew.DRAWSEGMENT()
    segment.SetShape(STROKE_T.S_ARC)
    segment.SetLayer(layer)
    segment.SetCenter  (arcCenterPoint)
    segment.SetArcStart(arcStartPoint)
    segment.SetAngle   (arcAngle)

    print("Add arc")
    board.Add(segment)

def drawSegment(board, startPoint, endPoint, layer=Layer.Edge_Cuts):
    '''
    Draw a straight line segment
    '''
    segment = pcbnew.DRAWSEGMENT()
    segment.SetShape(STROKE_T.S_SEGMENT)
    segment.SetLayer(layer)
    segment.SetStart(startPoint)
    segment.SetEnd  (endPoint)

    print("Add segment")
    board.Add(segment)

drawELCore(board, coreType, coreSize, core)

a = core['a']
d = np.ceil((a[0] + a[1]) / 2.54) * 2.54
#print(type(d))
d = getattr(d, "tolist", lambda: d)() # see https://stackoverflow.com/a/42923092
#print(type(d))

drawSegment(board, pcbnew.wxPointMM( d/2,  d/2), pcbnew.wxPointMM(-d/2,  d/2))
drawSegment(board, pcbnew.wxPointMM(-d/2,  d/2), pcbnew.wxPointMM(-d/2, -d/2))
drawSegment(board, pcbnew.wxPointMM(-d/2, -d/2), pcbnew.wxPointMM( d/2, -d/2))
drawSegment(board, pcbnew.wxPointMM( d/2, -d/2), pcbnew.wxPointMM( d/2,  d/2))

pcbnew.Refresh()

'''
Save board to given name using the Save method of the board instance
'''
board.Save('test.kicad_pcb')
