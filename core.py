# -*- coding: utf-8 -*-

#Create logging 
import logging



import json
import math
import os
import pcbnew
import kikit.panelize

if __name__ == '__main__':
    import util
else:
    from . import util


class Core():
    def __init__(self, board, module, core_type, core_size, config_file,
                 dimensions_file=None):
        '''Initialize
        '''
        if dimensions_file is not None:
            raise NotImplementedError(
                'Implement custom core dimensions file handling')
        else:
            dimensions_file = os.path.join(os.path.dirname(
                os.path.realpath(__file__)), "dimensions.json")
        logging.info("converter: load dimensions from {}".format(dimensions_file))
        print("converter: load dimensions from {}".format(dimensions_file))

        with open(dimensions_file, "r") as filename:
            cores = json.load(filename)

        self.board = board
        self.module = module
        
        logging.info(cores)
        #TODO also look into listing in for loop with python
        #self.core_types1 = [cores["Core Type"] for cores["Core Type"] in cores]    
        #self.core_types = self.core_types1[1:]
       
        #logging.info("Core Types:")
        #logging.info(self.core_types)
        
        #self.core_sizes = []
        #for i in range(len(self.core_types)):
        #self.core_sizes = [cores["EL"]["Core Size"] for cores["EL"]["Core Size"] in cores]
        #logging.info("Core Sizes:")
        #logging.info(self.core_sizes)
        
        self.core_type = core_type
        self.core_size = core_size

        self.core = cores[core_type][core_size]
        self.load_dimensions()
        self.load_design_rules()
        self.load_config()
        
        self.calculate_dimensions()


    def load_config(self, config_file=None):
        '''Load configuration

        Abstract method that needs to be implemented in derived classes.
        '''
        self.degree_per_segment = 10
        self.rad_per_segment = math.radians(self.degree_per_segment)

    def load_design_rules(self):
        '''Load design rules
        '''
        # TODO: Primary design parameters need to be called from net class, because they can be different from 
        #       design constraints, especially copper_sep and track_width
        #       These also exist in transformer Design Parameters()

        #Design rules constraints (preset) 
        design_settings = self.board.GetDesignSettings()
        #Copper
        #Minimum clearance
        min_copper_sep_mm = pcbnew.ToMM(design_settings.m_MinClearance)
        #Minimum track width
        min_track_width_mm = pcbnew.ToMM(design_settings.m_TrackMinWidth)
        #Minimum annular width (via)
        min_via_annular_width_mm = pcbnew.ToMM(design_settings.m_ViasMinAnnularWidth)
        #Minimum via diameter
        min_via_diameter_mm = pcbnew.ToMM(design_settings.m_ViasMinSize)
        #Copper to hole clearance
        copper_hole_sep_mm = pcbnew.ToMM(design_settings.m_HoleClearance)
        #Copper to edge clearance
        coppper_edge_sep_mm = pcbnew.ToMM(design_settings.m_CopperEdgeClearance)
        #Holes
        #Minimum through hole
        min_through_hole_mm = pcbnew.ToMM(design_settings.m_MinThroughDrill)
        #Hole to hole clearance
        hole_hole_sep_mm = pcbnew.ToMM(design_settings.m_HoleToHoleMin)

        #set parameters as frame for calculations
        self.mill_sep = coppper_edge_sep_mm
        self.copper_sep = min_copper_sep_mm
        self.via_width = min_via_diameter_mm
        self.via_drill = min_through_hole_mm
    
        #The following parameters are not callable through KiCad and thus need to be supplied directly
        #Milling tolerance
        self.mill_tol = 0.1

        
    def load_dimensions(self):
        '''Load core dimensions

        Abstract method that needs to be implemented in the derived class.
        '''
        raise NotImplementedError("Implement load_dimensions()")

    def calculate_dimensions(self):
        '''Calculate transformer dimensions

        Calculation of the transformer dimensions includes the coil geometry.
        '''
        raise NotImplementedError("Implement calculate_dimensions()")

    def draw(self):
        '''Draw the transformer

        Drawing of the transformer includes technical layers for cut out and
        silkscreen as well as on copper layers.
        '''
        raise NotImplementedError("Implement draw()")

    def draw_holes(self):
        raise NotImplementedError("Implement draw_holes()")

    def draw_helpers(self):
        raise NotImplementedError("Implement draw_helpers()")

    def segment_count(self, angle):
        '''Return count of segments per angle
        '''
        count = math.ceil(abs(angle)/self.rad_per_segment)

        return count


class ELT(Core):
    def __init__(self, board, module, core_type, core_size, config_file,
                 dimensions_file=None):
        super().__init__(board, module, core_type, core_size, config_file,
                         dimensions_file)

    
    def __repr__(self):
        return (
            f'ELT(board={self.board}, module={self.module}'
            f', core_type={self.core_type}, core_size={self.core_size}'
            # f', config_file={self.config_file}'
            # f', dimensions_file={self.dimensions_file}'
            f')'
        )

    def __str__(self):
        return f'{self.core_type}{self.core_size}'

    def load_config(self, config_file=None):
        super().load_config()

        if config_file is None:
        
            #PCB Parameters and their defaults, they're overwritten by the user interface inputs once given
            self.pcb_count = 1
            self.layer_count_per_pcb = 12

            self.enabled_cu_layers = [util.Layer.F_Cu, util.Layer.In1_Cu, util.Layer.In2_Cu, util.Layer.In3_Cu,
                                    util.Layer.In4_Cu, util.Layer.In5_Cu, util.Layer.In6_Cu, util.Layer.In7_Cu,
                                    util.Layer.In8_Cu, util.Layer.In9_Cu, util.Layer.In10_Cu, util.Layer.B_Cu]

            self.pcb_configuration = ['\PRIMARY', '\PRIMARY', '\SECONDARY', '\SECONDARY',
                                    '\PRIMARY', '\PRIMARY','\PRIMARY', '\PRIMARY',
                                    '\PRIMARY', '\PRIMARY','\PRIMARY', '\PRIMARY',]
            self.layers_netcode = [1, 1, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1]
            

            self.primary_layers_count = 10.0
         
            self.primary_windings_count = 150.0

            self.primary_layer_windings_count = math.ceil(
                self.primary_windings_count / self.primary_layers_count)
            self.primary_vias_inside_count = int(self.primary_layers_count/2)

            self.primary_track_width = 0.0
            self.primary_track_track_sep = 0.0

            #measurements for secondary geometry
            self.center_tab_length = 15.24
            self.center_tab_width = 7.366
            self.outer_tab_length = 15.24
            self.outer_tab_width = 4.826
            self.tab_sep = 0.254
        else:
            raise NotImplementedError("Implement loading config from file")

        self.degree_per_segment = 10
        self.rad_per_segment = math.radians(self.degree_per_segment)
    
    def load_dimensions(self):
        #load core measurements from json file
        self.outer_length = self.core["a"][0]
        self.outer_length_tol = self.core["a"][1]
        self.inner_length = self.core["e"][0]
        self.inner_length_tol = self.core["e"][1]
        self.width = self.core["c"][0]
        self.width_tol = self.core["c"][1]
        self.leg_length = self.core["f2"][0]
        self.leg_length_tol = self.core["f2"][1]
        self.leg_width = self.core["f1"][0]
        self.leg_width_tol = self.core["f1"][1]
        self.radius = self.core["r2"][0]

    def calculate_dimensions(self):
        # stadium dimensions
        self.max_leg_length = self.leg_length + self.leg_length_tol
        self.max_leg_width = self.leg_width + self.leg_width_tol

        self.stadium_length = self.max_leg_length + 2*self.mill_tol
        self.stadium_width = self.max_leg_width + 2*self.mill_tol

        self.focal_distance = self.max_leg_length - self.max_leg_width
        print("converter: focal distance = {: 9.6f}".format(
            self.focal_distance))

        self.focal_points = []

        self.focal_points.append(pcbnew.wxPointMM(self.focal_distance/2, 0))
        self.focal_points.append(pcbnew.wxPointMM(-self.focal_distance/2, 0))

        # rectangle dimensions
        self.max_outer_length = self.outer_length + self.outer_length_tol
        self.min_inner_length = self.inner_length - self.inner_length_tol

        self.min_inner_length_mill_tol = self.min_inner_length - 2*self.mill_tol

        self.max_width = self.width + self.width_tol

        self.rect_length = self.max_width + 2*self.mill_tol
        self.rect_width = (self.max_outer_length
                           - self.min_inner_length)/2 + 2*self.mill_tol
        self.rect_radius = self.radius + self.width_tol
        self.rect_offset_top = pcbnew.wxPointMM(
            0, (self.outer_length + self.inner_length)/4)
        self.rect_offset_bottom = pcbnew.wxPointMM(
            0, -(self.outer_length + self.inner_length)/4)

        # general coil dimensions
        self.winding_width = (self.min_inner_length - self.max_leg_width) / 2 \
            - 2*self.mill_tol - 2*self.mill_sep

        # primary coil dimensions
        self.primary_layer_windings_count = int(self.primary_windings_count
                                                / self.primary_layers_count)

        self.primary_track_width = (self.winding_width + self.copper_sep)     \
            / self.primary_layer_windings_count - self.copper_sep

        # contour track separation is the gap between the mill contour and the
        # center of the nearest track
        self.contour_track_sep = self.mill_sep + self.primary_track_width/2

        self.primary_track_track_sep = self.primary_track_width \
            + self.copper_sep

        # track bundle width is the width from the center of the innermost
        # track to the center of the outermost track in the primary track
        # bundle within one layer
        self.track_bundle_width = (self.primary_layer_windings_count-1) \
            * self.primary_track_track_sep
        print("converter: track bundle width = {} mm".format(
            self.track_bundle_width))

        # TODO: this variable needs a new name!
        self.start_track_radius = (
            self.max_leg_width + 2*(self.mill_tol + self.mill_sep)
            + self.primary_track_width)/2

        # distance between vias in the primary coil
        # TODO: check, if distance is large enough to account for track width
        #       greater than the via width! Quick fix: set via width equal to
        #       track width in this case.
        self.primary_via_via_distance = self.via_width + self.copper_sep

        # distance between vias and tracks in the primary coil
        self.primary_via_track_distance = self.via_width/2\
            + self.copper_sep \
            + self.primary_track_width/2

        # numnber of vias required to connect all primary coil layers on main
        self.primary_vias_inside_count = int(self.primary_layers_count/2) 
        self.primary_vias_outside_count = self.primary_vias_inside_count + 1 + (self.pcb_count-1)

        # number of vias required to connect all primary coil layers of additional pcbs
        self.primary_vias_inside_count_add_pcbs = int(self.primary_layers_count/2)
        self.primary_vias_outside_count_add_pcbs = self.primary_vias_inside_count_add_pcbs + 1 + (self.pcb_count-1)

        # distance between each of the inside primary coil vias and the nearest
        # focal point
        self.primary_vias_inside_radius = self.max_leg_width/2 \
            + self.mill_tol \
            + self.mill_sep \
            + self.via_width/2
        self.primary_vias_outside_radius = self.primary_vias_inside_radius \
            + 2*self.primary_via_track_distance \
            + (self.primary_layer_windings_count)*self.primary_track_track_sep
        # angle between two adjacent (benachbart) inside primary coil vias
        self.primary_vias_inside_angle = 2*math.asin(
            self.primary_via_via_distance/(2*self.primary_vias_inside_radius))
        print("converter: primary interior vias angle = {} deg".format(
            math.degrees(self.primary_vias_inside_angle)))

        # self.rad_per_segment = self.primary_vias_inside_angle
        # self.degree_per_segment = math.degrees(self.rad_per_segment)

        self.primary_vias_outside_angle = 2*math.asin(
            self.primary_via_via_distance/(2*self.primary_vias_outside_radius))
        print("converter: primary exterior vias angle = {} deg".format(
            math.degrees(self.primary_vias_outside_angle)))

        # angle of the first inside/outside primary coil via from the positive
        # x axis -- beta
        self.primary_vias_inside_start_angle = \
            (self.primary_vias_inside_count-1)/2 \
            * self.primary_vias_inside_angle
        self.primary_vias_outside_start_angle = \
            (self.primary_vias_outside_count-1)/2 \
            * self.primary_vias_outside_angle

        # angle of the first inside/outside primary coil via from the positive for additional pcbs
        # x axis -- beta
        self.primary_vias_inside_start_angle_add_pcbs = \
            (self.primary_vias_inside_count_add_pcbs-1)/2 \
            * self.primary_vias_inside_angle
        self.primary_vias_outside_start_angle_add_pcbs = \
            (self.primary_vias_outside_count_add_pcbs-1)/2 \
            * self.primary_vias_outside_angle

        # position of first inside primary via on the arc
        vector = pcbnew.VECTOR2I(pcbnew.wxPointMM(
            self.primary_vias_inside_radius, 0))
        vector = vector.Rotate(-self.primary_vias_inside_start_angle)
        self.primary_vias_inside_start = self.focal_points[0] \
            + vector.getWxPoint()

        # position of first inside primary via on the arc for additional pcbs
        vector = pcbnew.VECTOR2I(pcbnew.wxPointMM(
            self.primary_vias_inside_radius, 0))
        vector = vector.Rotate(-self.primary_vias_inside_start_angle_add_pcbs)
        self.primary_vias_inside_start_add_pcbs = self.focal_points[0] \
            + vector.getWxPoint()

        # position of first outside primary via on the arc
        vector = pcbnew.VECTOR2I(pcbnew.wxPointMM(
            self.primary_vias_outside_radius, 0))
        vector = vector.Rotate(-self.primary_vias_outside_start_angle)
        self.primary_vias_outside_start = self.focal_points[0] \
            + vector.getWxPoint()

        # position of first outside primary via on the arc for additional pcbs
        vector = pcbnew.VECTOR2I(pcbnew.wxPointMM(
            self.primary_vias_outside_radius, 0))
        vector = vector.Rotate(-self.primary_vias_outside_start_angle_add_pcbs)
        self.primary_vias_outside_start_add_pcbs = self.focal_points[0] \
            + vector.getWxPoint()

        self.primary_track_length_same = math.sqrt(
            self.primary_vias_inside_radius**2
            - (self.start_track_radius
               - self.primary_via_track_distance)**2)
        print("converter: primary track length same = {} mm".format(
            self.primary_track_length_same))

        self.primary_track_length_inbound = math.sqrt(
            self.primary_vias_inside_radius**2
            - (self.start_track_radius
               - self.primary_track_track_sep
               - self.primary_via_track_distance)**2)
        print("converter: primary track length inbound = {} mm".format(
            self.primary_track_length_inbound))

        self.primary_track_length_outbound = math.sqrt(
            self.primary_vias_inside_radius**2
            - (self.start_track_radius
               + self.primary_track_track_sep
               - self.primary_via_track_distance)**2)
        print("converter: primary track length outbound = {} mm".format(
            self.primary_track_length_outbound))

        self.theta_same = math.asin(self.primary_track_length_same
                                    / self.primary_vias_inside_radius)
        print("converter: theta same = {} deg".format(
            math.degrees(self.theta_same)))

        self.alpha_same = math.pi/2 - self.primary_vias_inside_start_angle \
            - self.theta_same
        print("converter: alpha same: = {} deg".format(
            math.degrees(self.alpha_same)))

        self.theta_inbound = math.asin(self.primary_track_length_inbound
                                       / self.primary_vias_inside_radius)
        print("converter: theta inbound = {} deg".format(
            math.degrees(self.theta_inbound)))

        self.alpha_inbound = math.pi/2 - self.primary_vias_inside_start_angle \
            - self.theta_inbound
        print("converter: alpha inbound: = {} deg".format(
            math.degrees(self.alpha_inbound)))

        self.theta_outbound = math.asin(self.primary_track_length_outbound
                                        / self.primary_vias_inside_radius)
        print("converter: theta outbound = {} deg".format(
            math.degrees(self.theta_outbound)))

        self.alpha_outbound = math.pi/2 \
            - self.primary_vias_inside_start_angle \
            - self.theta_outbound
        print("converter: alpha outbound: = {} deg".format(
            math.degrees(self.alpha_outbound)))

        # self.secondary_tab_width = (self.min_inner_length
        #                             - 2*self.mill_sep
        #                             - 2*self.copper_sep) / 3
        # print("converter: secondary connector width = {} mm".format(
        #     self.secondary_tab_width))

        self.secondary_outer_radius = self.min_inner_length/2 - self.mill_sep \
            - self.mill_tol
        self.secondary_inner_radius = self.stadium_width/2 + self.mill_sep

    def draw(self):
        primary_netcode = 1
        secondary_netcode = 3
        #Draws the main PCB trafo and if needed the additional ones
        #Create kikit panel class
        #self.pcb_panel = kikit.panelize.Panel("pcb-panels")

        #Draw main PCB
        self.draw_main_pcb()
        #Calculate offset for other PCBs w.r.t. main footprint center
        if self.pcb_count > 1:
            self.main_footprint_center = pcbnew.ToMM(self.module.GetPosition())
            self.main_footprint_center_x = self.main_footprint_center[0]
            self.main_footprint_center_y = self.main_footprint_center[1]
            self.main_pcb_bb = self.board.GetBoardEdgesBoundingBox()
            self.main_pcb_bb_bottom = pcbnew.ToMM(self.main_pcb_bb.GetBottom())
            self.distance_footprint_center_bb_bottom = self.main_pcb_bb_bottom - self.main_footprint_center_y 
            self.footprint_center_pcb_bb_bottom = pcbnew.wxPointMM(self.main_footprint_center_x,  self.main_footprint_center_y + self.distance_footprint_center_bb_bottom)
        
            #Add main PCB to kikit panel class if panelization is needed  
        #    self.pcb_panel.appendBoard(filename="/home/klara/.local/share/kicad/6.0/scripting/plugins/kiconv/test/converter-breakout/converter-breakout.kicad_pcb", 
        #                        destination=pcbnew.wxPointMM(0,0), sourceArea=main_pcb_bb)

        #Draw addidtional PCBs if PCB count is higher than 1
        if self.pcb_count > 1:
            for x in range(1, self.pcb_count):
                self.draw_more_pcbs(pcb=x)
                footprint = self.pcb
        #        footprint_area = self.pcb.GetBoundingBox()
        #        self.pcb_panel.appendBoard(filename="/home/klara/.local/share/kicad/6.0/scripting/plugins/kiconv/test/converter-breakout/converter-breakout.kicad_pcb", 
        #                    destination=pcbnew.wxPointMM(0,x*(self.max_outer_length+10)), sourceArea=footprint_area)
        #        connecting_tab = self.pcb_panel.boardSubstrate.tab(origin = pcbnew.wxPointMM(0,x*(self.max_outer_length+10)-self.max_outer_length/2), 
        #                    direction= util.to_vector(pcbnew.wxPointMM(0,2)), width=pcbnew.FromMM(3))
        #        self.pcb_panel.appendSubstrate(connecting_tab[0])
                
                #Delete helper vias
                for via in self.primary_vias_inside_add_pcbs_del:
                    via.DeleteStructure()
                
                for via in self.primary_vias_outside_add_pcbs_del:
                    via.DeleteStructure()

        self.draw_helpers()

    def draw_main_pcb(self):
        #Draw the main PCB
        primary_netcode = 1
        secondary_netcode = 3

        #Draw cutouts and place vias on main PCB
        #self.draw_holes()
        self.place_vias(primary_netcode=primary_netcode)

        #Parameter for via connected to primary track
        self.connected_via_main = 0
        #Parameter to know if track is inbound or outbound
        inbound_outbound = 1
        #Parameter to know whether or not to mirror secondary track
        mirror = 1
        
        #Draw tracks for main PCB
        for x in range(0, len(self.enabled_cu_layers)):
            
            #For layers designated for primary side
            if self.layers_netcode[x] == 1:
                #If inbound_outbound uneven draw_inbound
                if inbound_outbound % 2:
                    self.draw_inbound(self.connected_via_main, self.enabled_cu_layers[x], 1)
                    #Adjust inbound_outbound for next primary layer to be outbound
                    inbound_outbound = inbound_outbound + 1
                #If inbound_outbound even draw_outbound
                else:
                    self.draw_outbound(self.connected_via_main, self.enabled_cu_layers[x], 1)
                    #Adjust inbound_outbound for next primary layer to be inbound
                    inbound_outbound = inbound_outbound + 1
                    #Adjust index count for next via
                    self.connected_via_main = self.connected_via_main + 1

            #For layers designated for secondary side
            if self.layers_netcode[x] == 3:
                #If mirror is uneven don't mirror
                if mirror % 2:
                    self.draw_secondary(self.enabled_cu_layers[x], 3)
                    #Adjust mirror for next secondary layer to be inbound
                    mirror = mirror + 1
                #If secondary even mirror
                else:
                    self.draw_secondary(self.enabled_cu_layers[x], 3, True)
        
        #Add connection to driver electronics
        self.connect_vias_to_pads()
        print('ELT.draw_main_pcb: terminated')

    def draw_more_pcbs(self, pcb):
        primary_netcode = 1
        secondary_netcode = 3
        
        #Calculate minimum offset to main PCB trafo footprint center
        self.distance_pcbs = 2.0
        self.offset_first = self.footprint_center_pcb_bb_bottom + pcbnew.wxPointMM(0, self.distance_pcbs+1/2*self.min_inner_length_mill_tol)
        self.offset_follow = self.offset_first + pcbnew.wxPointMM(0, (pcb-1)*(self.min_inner_length_mill_tol+self.distance_pcbs))
        if pcb == 1:
            self.offset_pcbs =  self.offset_first
        else:
            self.offset_pcbs = self.offset_follow

        self.offset_items = self.offset_pcbs - pcbnew.wxPointMM(self.main_footprint_center_x, self.main_footprint_center_y)
        
        #Draw cutouts and playe vias on additional PCBs
        #self.draw_holes_more_pcbs(pcb)
        self.place_vias_more_pcbs(primary_netcode=primary_netcode, pcb=pcb)

        #Get additional footprint name by manipulating existing one of main PCB and loading accordingly
        self.module_name = self.module.GetValue()
        transformer_footprints = "Tranformer_EPCOS_PC95"
        stand_alone = "-Z_standalone"
        additional_footprint_name = transformer_footprints + self.module_name + stand_alone
        self.pcb = util.load_footprint(self.board, additional_footprint_name, self.offset_pcbs)

        #Parameter count for vias connected to according primary tracks
        #Via count for further PCBs
        if pcb == 1:
            connected_via = self.connected_via_main + 1
        else:
            connected_via = self.connected_via_main + 1 + (pcb-1)*(int((len(self.enabled_cu_layers)/2)+1))
        
        #Parameter to know if track is inbound or outbound
        inbound_outbound = 1

        #Draw tracks for additional PCBs
        for x in range(0, len(self.enabled_cu_layers)):

            #For layers designated for primary tracks
            if self.layers_netcode[x + pcb * len(self.enabled_cu_layers)] == 1:
                #If inbound_outbound uneven draw_inbound
                if inbound_outbound % 2:
                    self.draw_inbound_more_pcbs(connected_via, self.enabled_cu_layers[x], 1, pcb)
                    #Adjust inbound_outbound for next primary layer to be outbound
                    inbound_outbound = inbound_outbound + 1
                #If inbound_outbound even draw_outbound
                else:
                    self.draw_outbound_more_pcbs(connected_via, self.enabled_cu_layers[x], 1, pcb)
                    #Adjust inbound_outbound for next primary layer to be inbound
                    inbound_outbound = inbound_outbound + 1
                    #Adjust index count for next via
                    connected_via = connected_via + 1

        #Add connection to castellated holes on additional PCBS
        self.connect_vias_to_pads_more_pcbs(pcb = pcb, footprint = self.pcb)
        print('ELT.draw_more_pcbs: terminated')

    def draw_holes(self):
        '''Draw ELT and EL core milled holes
        '''
        print("converter: draw ELT core")

        # TODO: return segment objects to be able to delete them later
        # draw stadium mill contour
        util.draw_stadium(
            self.board, self.stadium_length, self.stadium_width,
            util.Layer.Edge_Cuts, 0.05, self.module.GetPosition(),
            self.module.GetOrientationDegrees()*10)

        # draw positive y position rounded rectangle mill contour
        util.draw_rounded_rectangle(
            self.board, self.rect_length, self.rect_width, self.rect_radius,
            util.Layer.Edge_Cuts, 0.05, self.module.GetPosition(),
            self.module.GetOrientationDegrees()*10,
            offset=self.rect_offset_top)

        # draw negative y position rounded rectangle mill contour
        util.draw_rounded_rectangle(
            self.board, self.rect_length, self.rect_width, self.rect_radius,
            util.Layer.Edge_Cuts, 0.05, self.module.GetPosition(),
            self.module.GetOrientationDegrees()*10,
            offset=self.rect_offset_bottom)
    
    def draw_holes_more_pcbs(self, pcb=1):
        #calculate necessary offset
        offset = self.offset_pcbs

        util.draw_stadium(
            board = self.board, length = self.stadium_length, width = self.stadium_width,
            layer = util.Layer.Edge_Cuts, segment_width = 0.05, center = self.module.GetPosition(),
            rotation_angle = self.module.GetOrientationDegrees()*10, offset = offset)
        

    def place_vias(self, primary_netcode=1, debug: bool = False):
        # add vias on inside of primary windings on main PCB
        # TODO: change interface such, that center and position are given
        self.primary_vias_inside = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_inside_start,
            self.primary_via_via_distance,
            self.primary_vias_inside_count,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset=self.module.GetPosition(),
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        # add vias on outside of primary windings on main PCB
        self.primary_vias_outside = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_outside_start,
            self.primary_via_via_distance,
            self.primary_vias_outside_count,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset=self.module.GetPosition(),
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        print('ELT.place_vias: terminated')

    def place_vias_more_pcbs(self, primary_netcode=1, pcb=1, debug: bool = False):
        
        # TODO: change interface such, that center and position are given
        # offset w.r.t. main footprint center
        offset = self.offset_items
        # add vias on inside of primary windings
        self.primary_vias_inside_add_pcbs = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_inside_start_add_pcbs,
            self.primary_via_via_distance,
            self.primary_vias_inside_count_add_pcbs,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset = offset,
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        # add vias on outside of primary windings
        self.primary_vias_outside_add_pcbs = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_outside_start_add_pcbs,
            self.primary_via_via_distance,
            self.primary_vias_outside_count_add_pcbs,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset=offset,
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        #add helper vias on outside of primary windings to delete later
        self.primary_vias_inside_add_pcbs_del = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_inside_start_add_pcbs,
            self.primary_via_via_distance,
            self.primary_vias_inside_count_add_pcbs,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset=pcbnew.wxPointMM(self.main_footprint_center_x, self.main_footprint_center_y),
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        #add helper vias on outside of primary windings
        self.primary_vias_outside_add_pcbs_del = util.add_vias_on_arc(
            self.board,
            self.focal_points[0],
            self.primary_vias_outside_start_add_pcbs,
            self.primary_via_via_distance,
            self.primary_vias_outside_count_add_pcbs,
            self.via_width,
            self.via_drill,
            netcode=primary_netcode,
            offset=pcbnew.wxPointMM(self.main_footprint_center_x, self.main_footprint_center_y),
            rotation_angle=self.module.GetOrientationDegrees()*10,
            debug=debug)

        print('ELT.place_vias_more_pcbs: terminated')


    def connect_vias_to_pads(self):
        '''Connect according vias to pads and connectors on main PCB
        '''
        offset = pcbnew.wxPointMM(0,0)
        #Primary connector pads positions
        main_pcb_connector = self.board.FindFootprintByReference("J1")
        main_pcb_connector_pad_upper = main_pcb_connector.FindPadByNumber("1")
        main_pcb_connector_pad_position_upper = main_pcb_connector_pad_upper.GetPosition()
        main_pcb_connector_pad_bottom = main_pcb_connector.FindPadByNumber("3")
        main_pcb_connector_pad_position_bottom = main_pcb_connector_pad_bottom.GetPosition()
        #Footprint pads positions
        module_pad_upper_right = self.module.FindPadByNumber("7")
        module_pad_position_upper_right = module_pad_upper_right.GetPosition()
        module_pad_bottom_right = self.module.FindPadByNumber("1")
        module_pad_position_bottom_right = module_pad_bottom_right.GetPosition()
        #Vias positions
        main_pcb_via_position_upper = self.primary_vias_outside[0].GetPosition()
        main_pcb_via_position_bottom = self.primary_vias_outside[self.primary_vias_outside_count - 1].GetPosition()
        
        #connection for main PCB if main is only PCB
        if self.pcb_count ==1:
            upper_layer_connect = []
            bottom_layer_connect = []

            #Via to connector pad connection upper layer
            out_of_via_upper = main_pcb_via_position_upper + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
            out_of_via_upper_y = out_of_via_upper[1]
            main_pcb_connector_pad_position_upper_y = main_pcb_connector_pad_position_upper[1]
            out_via_pad_upper_y_diff = pcbnew.ToMM(main_pcb_connector_pad_position_upper_y - out_of_via_upper_y)
            out_via_pad_angle_upper = out_of_via_upper + pcbnew.wxPointMM(-out_via_pad_upper_y_diff, out_via_pad_upper_y_diff)

            upper_layer_connect.append(main_pcb_via_position_upper)
            upper_layer_connect.append(out_of_via_upper)
            upper_layer_connect.append(out_via_pad_angle_upper)
            upper_layer_connect.append(main_pcb_connector_pad_position_upper)

            #Via to connector pad connection bottom layer
            out_of_via_bottom = main_pcb_via_position_bottom + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
            out_of_via_bottom_y = out_of_via_bottom[1]
            main_pcb_connector_pad_position_bottom_y = main_pcb_connector_pad_position_bottom[1]
            out_via_pad_bottom_y_diff = pcbnew.ToMM(main_pcb_connector_pad_position_bottom_y - out_of_via_bottom_y)
            out_via_pad_angle_bottom = out_of_via_bottom + pcbnew.wxPointMM(out_via_pad_bottom_y_diff, out_via_pad_bottom_y_diff)

            bottom_layer_connect.append(main_pcb_via_position_bottom)
            bottom_layer_connect.append(out_of_via_bottom)
            bottom_layer_connect.append(out_via_pad_angle_bottom)
            bottom_layer_connect.append(main_pcb_connector_pad_position_bottom)

            #Draw according tracks
            util.add_tracks(board = self.board, points = upper_layer_connect, width = self.primary_track_width,
            layer = util.Layer.F_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center=offset)
            util.add_tracks(board = self.board, points = bottom_layer_connect, width = self.primary_track_width,
            layer = util.Layer.B_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center=offset)

        #connection for main PCB if additional PCBs exist
        else:
            upper_layer_connect_1 =[]
            upper_layer_connect_2 = []
            bottom_layer_connect = []
            pad_bridge = []

            #Via to pad connection upper layer
            #Connector to via of first primary layer connection (in)
            out_of_via_upper = main_pcb_via_position_upper + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
            out_of_via_upper_y = out_of_via_upper[1]
            main_pcb_connector_pad_position_upper_y = main_pcb_connector_pad_position_upper[1]
            out_via_pad_upper_y_diff = pcbnew.ToMM(main_pcb_connector_pad_position_upper_y - out_of_via_upper_y)
            out_via_pad_angle_upper = out_of_via_upper + pcbnew.wxPointMM(-out_via_pad_upper_y_diff, out_via_pad_upper_y_diff)
            #Main footprint pad to connector connection (out)
            module_pad_lower_right_x_diff =  pcbnew.ToMM(main_pcb_connector_pad_position_bottom[0]- module_pad_position_bottom_right[0])
            module_pad_lower_right_angle = module_pad_position_bottom_right + pcbnew.wxPointMM(module_pad_lower_right_x_diff, -module_pad_lower_right_x_diff)

            upper_layer_connect_1.append(main_pcb_via_position_upper)
            upper_layer_connect_1.append(out_of_via_upper)
            upper_layer_connect_1.append(out_via_pad_angle_upper)
            upper_layer_connect_1.append(main_pcb_connector_pad_position_upper)

            upper_layer_connect_2.append(main_pcb_connector_pad_position_bottom)
            upper_layer_connect_2.append(module_pad_lower_right_angle)
            upper_layer_connect_2.append(module_pad_position_bottom_right)

            #Via to main footprint pad connection bottom layer
            main_pcb_via_position_bottom = self.primary_vias_outside[self.connected_via_main].GetPosition()

            out_of_via_bottom = main_pcb_via_position_bottom + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
            out_of_via_bottom_x = out_of_via_bottom[0]
            module_pad_upper_right_x = module_pad_position_upper_right[0]
            out_via_module_upper_pad_x_diff = pcbnew.ToMM(module_pad_upper_right_x - out_of_via_bottom_x)
            out_via_module_pad_angle_bottom = out_of_via_bottom + pcbnew.wxPointMM(out_via_module_upper_pad_x_diff, -out_via_module_upper_pad_x_diff)

            bottom_layer_connect.append(main_pcb_via_position_bottom)
            bottom_layer_connect.append(out_of_via_bottom)
            bottom_layer_connect.append(out_via_module_pad_angle_bottom)
            bottom_layer_connect.append(module_pad_position_upper_right)
            
            #Bridge between pads for more than two PCBs
            if self.pcb_count == 3:
                pad_bridge.append(module_pad_position_upper_right)
                pad_bridge.append(module_pad_position_bottom_right)

                #draw according track
                util.add_tracks(board = self.board, points = pad_bridge, width = self.primary_track_width,
                layer = util.Layer.In1_Cu, netcode = 1, vector = self.module.GetPosition(),
                angle = self.module.GetOrientation(), rotation_center=offset)               

                
            #draw according tracks
            util.add_tracks(board = self.board, points = upper_layer_connect_1, width = self.primary_track_width,
            layer = util.Layer.F_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center=offset)

            util.add_tracks(board = self.board, points = upper_layer_connect_2, width = self.primary_track_width,
            layer = util.Layer.F_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center=offset)

            util.add_tracks(board = self.board, points = bottom_layer_connect, width = self.primary_track_width,
            layer = util.Layer.B_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center=offset)

        print('ELT.connect_vias_to_pads: terminated')

    def connect_vias_to_pads_more_pcbs(self, pcb, footprint):
        '''Connect according vias to pads through tracks on not main pcbs
        '''
        upper_layer_connect =[]
        bottom_layer_connect =[]
        offset = self.offset_items
        #Vias that need to be connected to the castellated holes
        start_via = int(pcb * self.layer_count_per_pcb/2 + (pcb-1)) 
        end_via = int(start_via + self.layer_count_per_pcb/2)

        #Get positions of footprints pad and via for upper layer 
        pad_upper = footprint.FindPadByNumber("4")
        pad_upper_size = pcbnew.ToMM(pad_upper.GetSizeX()/2)
        pad_position_upper = pad_upper.GetPosition()
        pad_position_upper_x = pad_position_upper[0]
        via_position_upper = self.primary_vias_outside_add_pcbs[start_via].GetPosition()
        #Via to pad connection upper layer
        out_of_via_upper = via_position_upper + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
        out_of_pad_upper = pad_position_upper - pcbnew.wxPointMM(pad_upper_size+self.copper_sep+self.primary_track_width/2, 0)
        out_of_via_upper_x = out_of_via_upper[0]
        out_of_pad_upper_x = out_of_pad_upper[0]
        out_pad_pad_upper_x_diff = pcbnew.ToMM(pad_position_upper_x - out_of_pad_upper_x)
        out_of_pad_upper_angle = pad_position_upper + pcbnew.wxPointMM(-out_pad_pad_upper_x_diff, out_pad_pad_upper_x_diff)
        out_via_pad_upper_x_diff = pcbnew.ToMM(out_of_pad_upper_x - out_of_via_upper_x)
        out_via_pad_angle_upper = out_of_via_upper + pcbnew.wxPointMM(out_via_pad_upper_x_diff, -out_via_pad_upper_x_diff)

        upper_layer_connect.append(via_position_upper)
        upper_layer_connect.append(out_of_via_upper)
        upper_layer_connect.append(out_via_pad_angle_upper)
        upper_layer_connect.append(out_of_pad_upper_angle)
        upper_layer_connect.append(pad_position_upper)

        #Get positions of footprints pad and via for bottom layer
        pad_bottom = footprint.FindPadByNumber("1")
        pad_bottom_size = pcbnew.ToMM(pad_bottom.GetSizeX()/2)
        pad_position_bottom = pad_bottom.GetPosition()
        pad_position_bottom_x = pad_position_bottom[0]
        via_position_bottom = self.primary_vias_outside_add_pcbs[end_via].GetPosition()
        #Via to pad connection bottom layer
        out_of_via_bottom = via_position_bottom + pcbnew.wxPointMM(self.via_width/2+self.copper_sep+self.primary_track_width/2, 0)
        out_of_pad_bottom = pad_position_bottom - pcbnew.wxPointMM(pad_bottom_size+self.copper_sep+self.primary_track_width/2, 0)
        out_of_via_bottom_x = out_of_via_bottom[0]
        out_of_pad_bottom_x = out_of_pad_bottom[0]
        out_pad_pad_bottom_x_diff = pcbnew.ToMM(pad_position_bottom_x - out_of_pad_bottom_x)
        out_of_pad_bottom_angle = pad_position_bottom + pcbnew.wxPointMM(-out_pad_pad_bottom_x_diff, -out_pad_pad_bottom_x_diff)
        out_via_pad_bottom_x_diff = pcbnew.ToMM(out_of_pad_bottom_x - out_of_via_bottom_x)
        out_via_pad_angle_buttom = out_of_via_bottom + pcbnew.wxPointMM(out_via_pad_bottom_x_diff, out_via_pad_bottom_x_diff)

        bottom_layer_connect.append(via_position_bottom)
        bottom_layer_connect.append(out_of_via_bottom)
        bottom_layer_connect.append(out_via_pad_angle_buttom)
        bottom_layer_connect.append(out_of_pad_bottom_angle)
        bottom_layer_connect.append(pad_position_bottom)

        #Draw according tracks
        util.add_tracks(board = self.board, points = upper_layer_connect, width = self.primary_track_width,
            layer = util.Layer.F_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center = offset)
        util.add_tracks(board = self.board, points = bottom_layer_connect, width = self.primary_track_width,
            layer = util.Layer.B_Cu, netcode = 1, vector = self.module.GetPosition(),
            angle = self.module.GetOrientation(), rotation_center = offset)

        print('ELT.connect_vias_to_pads_more_pcbs: terminated')

    def secondary_geometry(self):
        '''Output points for secondary coil geometry based on copper pour
        '''
        outer_tab_inner_y = self.center_tab_width/2 + self.tab_sep
        outer_tab_outer_y = outer_tab_inner_y + self.outer_tab_width

        corner_center = self.rect_offset_bottom + pcbnew.wxPointMM(
            self.rect_radius - self.rect_length/2,
            -self.rect_radius + self.rect_width/2)
        
        # start at left outer tab corner
        points = [pcbnew.wxPointMM(-self.outer_tab_length, -outer_tab_outer_y)]

        if outer_tab_outer_y < self.secondary_outer_radius:
            # y coordinate of outer tab is within the transformer core
            start = pcbnew.wxPoint(
                self.focal_points[1].x,
                pcbnew.FromMM(self.secondary_outer_radius))
            angle = math.pi
            count = self.segment_count(angle)
            arc = util.points_on_arc(self.focal_points[1], start, angle, count)
            target = pcbnew.wxPointMM(
                pcbnew.ToMM(self.focal_points[1].x) - math.sqrt(
                    self.secondary_outer_radius**2 - outer_tab_outer_y**2),
                -outer_tab_outer_y)
            points.extend(
                util.split_at_nearest_segment(
                    target, arc, how='y', keep_before=False))

        elif outer_tab_outer_y > self.secondary_outer_radius:
            vector = (util.to_vector(corner_center)
                      - util.to_vector(self.focal_points[1]))
            gamma = math.asin(
                pcbnew.FromMM(self.rect_radius + self.mill_sep)
                / vector.EuclideanNorm())
            alpha = vector.Angle() - gamma
            points.append(
                pcbnew.wxPoint(
                    self.focal_points[1].x - math.tan(abs(alpha) - math.pi/2)
                    * pcbnew.FromMM(outer_tab_outer_y),
                    -pcbnew.FromMM(outer_tab_outer_y)))

            # arc around slot radius
            rho = int(
                math.sqrt(
                    vector.EuclideanNorm()**2
                    - pcbnew.FromMM(self.rect_radius + self.mill_sep)**2))
            start = self.focal_points[1] \
                + pcbnew.VECTOR2I(rho, 0).Rotate(alpha).getWxPoint()
            angle = -(math.pi + alpha)
            count = self.segment_count(angle)
            arc = util.points_on_arc(corner_center, start, angle, count)

            points.extend(arc)
        
        # around right vias on the outside
        points.extend(
            self.points_around_vias(
                self.focal_points[0],
                util.to_module_frame(
                    self.module, util.to_point(self.primary_vias_inside)),
                self.secondary_outer_radius,
                self.secondary_outer_radius + self.primary_via_via_distance
                + self.primary_track_track_sep,
                self.secondary_outer_radius))

        # arc segment about left focal point on the outside
        if self.center_tab_width/2 < self.secondary_outer_radius:
            start = util.to_vector(points[-1])
            start.x = self.focal_points[1].x
            angle = math.pi
            count = self.segment_count(angle)
            arc = util.points_on_arc(self.focal_points[1], start, angle, count)
            target = pcbnew.wxPointMM(
                pcbnew.ToMM(self.focal_points[1].x) - math.sqrt(
                    self.secondary_outer_radius**2
                    - self.center_tab_width**2/4),
                self.center_tab_width/2)
            points.extend(
                util.split_at_nearest_segment(
                    target, arc, how='y', keep_before=True))

        # straight segments to form phalange (tm)
        vector = util.to_vector(points[-1])
        vector.x = pcbnew.FromMM(-self.center_tab_length)
        points.append(util.to_point(vector))

        vector.y = -vector.y
        points.append(util.to_point(vector))

        if self.center_tab_width/2 < self.secondary_inner_radius:
            start = pcbnew.wxPoint(
                self.focal_points[1].x,
                -pcbnew.FromMM(self.secondary_inner_radius))
            angle = math.pi
            count = self.segment_count(angle)
            arc = util.points_on_arc(
                self.focal_points[1], start, -angle, count)
            target = pcbnew.wxPointMM(
                pcbnew.ToMM(self.focal_points[1].x) - math.sqrt(
                    self.secondary_inner_radius**2
                    - self.center_tab_width**2/4),
                -self.center_tab_width/2)
            points.extend(
                util.split_at_nearest_segment(
                    target, arc, how='y', keep_before=False))

        elif self.center_tab_width/2 > self.secondary_inner_radius:
            y = abs(corner_center.y) - pcbnew.FromMM(self.center_tab_width/2)
            R = pcbnew.FromMM(
                self.secondary_outer_radius - self.secondary_inner_radius
                + 2*self.mill_sep + self.rect_radius)
            x = round(math.sqrt(R**2 - y**2))

            start = pcbnew.VECTOR2I(
                corner_center.x - x, pcbnew.FromMM(-self.center_tab_width/2))
            angle = -math.atan(x/y)
            count = self.segment_count(angle)
            points.extend(
                util.points_on_arc(
                    corner_center, start, angle, count))

            y = pcbnew.FromMM(self.stadium_width/2)
            R = pcbnew.FromMM(self.secondary_inner_radius)
            x = round(math.sqrt(R**2 - y**2))

            start = pcbnew.VECTOR2I(
                self.focal_points[1].x - x, -y)
            angle = -(math.pi - math.atan(x/y))
            count = self.segment_count(angle)
            points.extend(
                util.points_on_arc(
                    self.focal_points[1], start, angle, count))

        points.extend(
            self.points_around_vias(
                self.focal_points[0],
                util.to_module_frame(
                    self.module, util.to_point(self.primary_vias_inside)),
                self.secondary_inner_radius,
                self.secondary_inner_radius + self.primary_via_via_distance,
                self.secondary_inner_radius)[::-1])

        if outer_tab_inner_y < self.secondary_inner_radius:
            start = pcbnew.wxPoint(
                self.focal_points[1].x,
                -pcbnew.FromMM(self.secondary_inner_radius))
            angle = math.pi
            count = self.segment_count(angle)
            arc = util.points_on_arc(
                self.focal_points[1], start, -angle, count)
            target = pcbnew.wxPointMM(
                pcbnew.ToMM(self.focal_points[1].x) - math.sqrt(
                    self.secondary_inner_radius**2
                    - outer_tab_inner_y**2),
                -outer_tab_inner_y)
            points.extend(
                util.split_at_nearest_segment(
                    target, arc, how='y', keep_before=True))

        elif self.center_tab_width/2 > self.secondary_inner_radius:
            R = pcbnew.FromMM(
                self.secondary_outer_radius - self.secondary_inner_radius
                + self.rect_radius + self.mill_sep)
            y = abs(corner_center.y) - pcbnew.FromMM(outer_tab_inner_y)
            x = round(math.sqrt(R**2 - y**2))

            start = pcbnew.wxPoint(
                corner_center.x, corner_center.y + R)
            angle = math.atan(x/y)
            count = self.segment_count(angle)
            points.extend(
                util.points_on_arc(
                    corner_center, start, angle, count))
        
        points.append(
            pcbnew.wxPointMM(-self.outer_tab_length, -outer_tab_inner_y))

        return util.to_global_frame(self.module, points)

    def draw_inbound(self, index=0, layer=util.Layer.F_Cu, netcode=1):
        '''Draw inbound spiral of primary coil for main PCB
        '''
        center_right = util.to_vector(self.focal_points[0])
        center_left = util.to_vector(self.focal_points[1])

        inside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_inside))
        outside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_outside))

        start_radius = self.start_track_radius + self.track_bundle_width \
            + self.primary_track_track_sep
        mid_radius = self.start_track_radius + self.track_bundle_width \
            + self.via_width + self.copper_sep + self.primary_track_track_sep
        end_radius = self.start_track_radius + self.track_bundle_width

        in_via = outside_vias[index]
        out_via = inside_vias[index]

        vector = (util.to_vector(center_right) - util.to_vector(in_via)

                  ).Resize(pcbnew.FromMM(self.primary_via_track_distance))
        entrance = in_via + util.to_point(vector)

        # lead-in
        points = self.points_around_vias(
            center_right, inside_vias, start_radius, mid_radius, end_radius)

        segment, index = util.get_nearest_segment(entrance, points)

        p = util.line_intersection(segment, [in_via, entrance])

        clipped_points = points[index+1:]

        points = [util.to_point(in_via)]
        points.append(p)
        points.extend(clipped_points)

        for i in range(self.primary_layer_windings_count):
            # arced tracks around left focal point
            start = points[-1]
            start.x = center_left.x
            points.extend(
                util.points_on_arc(
                    center_left,
                    start,
                    math.pi,
                    self.segment_count(math.pi)))

            # points around vias
            last = self.points_around_vias(
                center_right,
                inside_vias,
                start_radius - (i+1)*self.primary_track_track_sep,
                mid_radius - (i+1)*self.primary_track_track_sep,
                end_radius - (i+1)*self.primary_track_track_sep)

            if i == self.primary_layer_windings_count - 1:
                vector = util.to_vector(out_via) - util.to_vector(center_right)
                vector = vector.Resize(pcbnew.FromMM(
                    self.primary_via_track_distance))
                entrance = out_via + util.to_point(vector)

                segment, index = util.get_nearest_segment(entrance, last)

                p = util.line_intersection(segment, [out_via, entrance])

                last = last[0:index+1]
                last.append(p)
                last.append(util.to_point(out_via))

            points.extend(last)
        
        #Add according tracks
        tracks = util.add_tracks(
            self.board, points, self.primary_track_width,
            layer, netcode, self.module.GetPosition(),
            self.module.GetOrientation())
        
        return tracks

    def draw_inbound_more_pcbs(self, index=0, layer=util.Layer.F_Cu, netcode=1, pcb=1):
        '''Draw inbound spiral of primary coil for additional PCBs
        '''
        #Info: Tracks are drawn around center of main pcb and then moved by offset
        offset = self.offset_items

        center_right = util.to_vector(self.focal_points[0])
        center_left = util.to_vector(self.focal_points[1])

        inside_vias= util.to_module_frame(
            self.module, util.to_point(self.primary_vias_inside_add_pcbs_del))
        outside_vias= util.to_module_frame(
            self.module, util.to_point(self.primary_vias_outside_add_pcbs_del))

        start_radius = self.start_track_radius + self.track_bundle_width \
            + self.primary_track_track_sep
        mid_radius = self.start_track_radius + self.track_bundle_width \
            + self.via_width + self.copper_sep + self.primary_track_track_sep
        end_radius = self.start_track_radius + self.track_bundle_width

        in_via = util.to_point(outside_vias[index])
        #out_via = util.to_point(inside_vias[index-1]) 
        out_via = util.to_point(inside_vias[index-pcb]) 

        vector = (util.to_vector(center_right) - util.to_vector(in_via)

                  ).Resize(pcbnew.FromMM(self.primary_via_track_distance))
        entrance = in_via + util.to_point(vector)

        # lead-in
        points = self.points_around_vias(
            center_right, inside_vias, start_radius, mid_radius, end_radius)

        segment, index = util.get_nearest_segment(entrance, points)

        p = util.line_intersection(segment, [in_via, entrance])

        clipped_points = points[index+1:]

        points = [util.to_point(in_via)]
        points.append(p)
        points.extend(clipped_points)

        for i in range(self.primary_layer_windings_count):
            # arced tracks around left focal point
            start = points[-1]
            start.x = center_left.x
            points.extend(
                util.points_on_arc(
                    center_left,
                    start,
                    math.pi,
                    self.segment_count(math.pi)))

            # points around vias
            last = self.points_around_vias(
                center_right,
                inside_vias,
                start_radius - (i+1)*self.primary_track_track_sep,
                mid_radius - (i+1)*self.primary_track_track_sep,
                end_radius - (i+1)*self.primary_track_track_sep)

            if i == self.primary_layer_windings_count - 1:
                vector = util.to_vector(out_via) - util.to_vector(center_right)
                vector = vector.Resize(pcbnew.FromMM(
                    self.primary_via_track_distance))
                entrance = out_via + util.to_point(vector)

                segment, index = util.get_nearest_segment(entrance, last)

                p = util.line_intersection(segment, [out_via, entrance])

                last = last[0:index+1]
                last.append(p)
                last.append(util.to_point(out_via))

            points.extend(last)
        
        #Add according tracks
        tracks_more_pcbs = util.add_tracks(
            board = self.board, points = points, width = self.primary_track_width,
            layer = layer, netcode = netcode, vector = self.module.GetPosition() + offset,
            angle = self.module.GetOrientation(), rotation_center = offset)

        return tracks_more_pcbs

    def draw_outbound(self, index=0, layer=util.Layer.B_Cu, netcode=1):
        '''Draw outbound spiral of primary coil for main PCB
        '''
        center_right = self.focal_points[0]
        center_left = self.focal_points[1]

        inside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_inside))
        outside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_outside))

        start_radius = self.start_track_radius
        mid_radius = self.start_track_radius + self.via_width + self.copper_sep
        end_radius = self.start_track_radius

        in_via = inside_vias[index]
        out_via = outside_vias[index+1]

        vector = util.to_vector(in_via) - util.to_vector(center_right)
        vector = vector.Resize(pcbnew.FromMM(self.primary_via_track_distance))
        entrance = in_via + util.to_point(vector)

        # lead-in
        # TODO: finish this using the method where we clip points
        points = self.points_around_vias(
            center_right, inside_vias, start_radius, mid_radius, end_radius)

        segment, index = util.get_nearest_segment(entrance, points)

        p = util.line_intersection(segment, [in_via, entrance])

        clipped_points = points[index+1:]

        points = [util.to_point(in_via)]
        points.append(p)
        points.extend(clipped_points)

        for i in range(self.primary_layer_windings_count):
            # arced tracks around left focal point
            start = points[-1]
            start.x = center_left.x
            points.extend(
                util.points_on_arc(
                    center_left,
                    start,
                    math.pi,
                    self.segment_count(math.pi)))

            # points around vias
            last = self.points_around_vias(
                center_right,
                inside_vias,
                start_radius + (i)*self.primary_track_track_sep,
                mid_radius + (i+1)*self.primary_track_track_sep,
                end_radius + (i+1)*self.primary_track_track_sep)

            if i == self.primary_layer_windings_count - 1:
                vector = util.to_vector(center_right) - util.to_vector(out_via)
                vector = vector.Resize(pcbnew.FromMM(
                    self.primary_via_track_distance))
                entrance = out_via + util.to_point(vector)

                segment, index = util.get_nearest_segment(entrance, last)

                p = util.line_intersection(segment, [out_via, entrance])

                last = last[0:index+1]
                last.append(p)
                last.append(util.to_point(out_via))

            points.extend(last)

        #Add according tracks
        tracks = util.add_tracks(
            self.board, points, self.primary_track_width, layer,
            netcode, self.module.GetPosition(), self.module.GetOrientation())

        return tracks

    def draw_outbound_more_pcbs(self, index=0, layer=util.Layer.B_Cu, netcode=1, pcb=1):
        '''Draw outbound spiral of primary coil for additional PCBs
        '''
        offset = self.offset_items

        center_right = self.focal_points[0]
        center_left = self.focal_points[1]

        inside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_inside_add_pcbs_del))
        outside_vias = util.to_module_frame(
            self.module, util.to_point(self.primary_vias_outside_add_pcbs_del))

        start_radius = self.start_track_radius
        mid_radius = self.start_track_radius + self.via_width + self.copper_sep
        end_radius = self.start_track_radius

        #in_via = inside_vias[index-1]
        in_via = inside_vias[index-pcb]
        out_via = outside_vias[index+1]

        vector = util.to_vector(in_via) - util.to_vector(center_right)
        vector = vector.Resize(pcbnew.FromMM(self.primary_via_track_distance))
        entrance = in_via + util.to_point(vector)

        # lead-in
        # TODO: finish this using the method where we clip points
        points = self.points_around_vias(
            center_right, inside_vias, start_radius, mid_radius, end_radius)

        segment, index = util.get_nearest_segment(entrance, points)

        p = util.line_intersection(segment, [in_via, entrance])

        clipped_points = points[index+1:]

        points = [util.to_point(in_via)]
        points.append(p)
        points.extend(clipped_points)

        for i in range(self.primary_layer_windings_count):
            # arced tracks around left focal point
            start = points[-1]
            start.x = center_left.x
            points.extend(
                util.points_on_arc(
                    center_left,
                    start,
                    math.pi,
                    self.segment_count(math.pi)))

            # points around vias
            last = self.points_around_vias(
                center_right,
                inside_vias,
                start_radius + (i)*self.primary_track_track_sep,
                mid_radius + (i+1)*self.primary_track_track_sep,
                end_radius + (i+1)*self.primary_track_track_sep)

            if i == self.primary_layer_windings_count - 1:
                vector = util.to_vector(center_right) - util.to_vector(out_via)
                vector = vector.Resize(pcbnew.FromMM(
                    self.primary_via_track_distance))
                entrance = out_via + util.to_point(vector)

                segment, index = util.get_nearest_segment(entrance, last)

                p = util.line_intersection(segment, [out_via, entrance])

                last = last[0:index+1]
                last.append(p)
                last.append(util.to_point(out_via))

            points.extend(last)

        #Add according tracks
        tracks_more_pcbs = util.add_tracks(
            board = self.board, points = points, width = self.primary_track_width,
            layer = layer, netcode = netcode, vector = self.module.GetPosition()+ offset,
            angle = self.module.GetOrientation(), rotation_center = offset)

        return tracks_more_pcbs 


    def draw_secondary(self, layer=util.Layer.F_Cu, netcode=0, mirror=False):
        # get secondary coil geometry
        points = self.secondary_geometry()
        filler = pcbnew.ZONE_FILLER(self.board)
        # draw coil defined by copper pour on layer In5.Cu
        shape_poly = pcbnew.SHAPE_POLY_SET()
        shape_poly.NewOutline()

        for point in points:
            if mirror:
                point = util.to_module_frame(self.module, point)
                point.y = -point.y
                point = util.to_global_frame(self.module, point)
            shape_poly.Append(util.to_vector(point))

        shape_poly.thisown = 0
        zone = pcbnew.ZONE(self.board)
        zone.SetOutline(shape_poly)
        zone.SetLayer(layer)
        zone.SetNetCode(netcode)
        #zone.SetIsKeepout(keepout)
        zone.thisown = 0
        self.board.Add(zone)
        zones =self.board.Zones()
        filler.Fill(zones)
        # draw coil defined by copper pour on layer In6.Cu and mirror about x
        shape_poly = pcbnew.SHAPE_POLY_SET()
        shape_poly.NewOutline()
        
    def draw_helpers(
            self,
            layer: util.Layer = util.Layer.Eco2_User,
            segment_width: float = 0.02,
            debug: bool = False):
        '''Draw helper lines
        '''
        radius = 0.2

        center_right = util.to_vector(self.focal_points[0])
        center_left = util.to_vector(self.focal_points[-1])

        position = self.module.GetPosition()
        orientation = self.module.GetOrientation()

        # focal points
        util.draw_circle(
            self.board, center_right, radius, layer, segment_width, position,
            orientation,  debug=debug)
        util.draw_circle(
            self.board, center_left, radius, layer, segment_width, position,
            orientation, debug=debug)

        # nominal stadium dimensions
        util.draw_stadium(
            self.board, self.leg_length, self.leg_width, layer, segment_width,
            position, orientation, debug=debug)

        # maximum stadium dimensions
        util.draw_stadium(
            self.board, self.max_leg_length, self.max_leg_width, layer,
            segment_width, position, orientation, debug=debug)

        # maximum stadium dimensions plus two times mill tolerance
        util.draw_stadium(
            self.board, self.stadium_length, self.stadium_width, layer,
            segment_width, position, orientation, debug=debug)

        # maximum stadium dimensions plus two times mill tolerance plus two
        # times contour separation
        util.draw_stadium(
            self.board,
            self.max_leg_length + 2*(self.mill_tol + self.mill_sep),
            self.max_leg_width + 2*(self.mill_tol + self.mill_sep), layer,
            segment_width, position, orientation, debug=debug)

        # lines from primary inside vias
        for via in self.primary_vias_inside:
            start = util.to_vector(
                util.to_module_frame(self.module, via.GetPosition()))
            vector = (start - center_right).Resize(
                pcbnew.FromMM(self.primary_via_track_distance))
            end = start + vector
            util.draw_segment(
                self.board, start, end, layer, segment_width, position,
                orientation, debug=debug)

        # lines from primary outside vias
        for via in self.primary_vias_outside:
            start = util.to_vector(
                util.to_module_frame(self.module, via.GetPosition()))
            vector = (center_right - start).Resize(
                pcbnew.FromMM(self.primary_via_track_distance))
            end = start + vector
            util.draw_segment(
                self.board, start, end, layer, segment_width, position,
                orientation, debug=debug)

        # circle around first inside primary via
        util.draw_circle(
            self.board, util.to_module_frame(
                self.module, self.primary_vias_inside[0].GetPosition()),
            self.primary_via_track_distance, layer, segment_width, position,
            orientation, debug=debug)

        # circle around right focal point with radius of first track center
        util.draw_circle(
            self.board, center_right, self.start_track_radius, layer,
            segment_width, position, orientation, debug=debug)

        util.draw_segment(
            self.board, center_right,
            self.focal_points[0] + pcbnew.wxPointMM(
                0, -self.start_track_radius),
            layer, segment_width, position, orientation, debug=debug)

        
    def points_around_vias(
            self, center, vias, start_radius, mid_radius, end_radius,
            debug: bool = False):
        '''Return points around vias
        '''
        center = util.to_vector(center)

        via1 = util.to_vector(vias[0])
        via2 = util.to_vector(vias[-1])

        start_radius = pcbnew.FromMM(start_radius)
        mid_radius = pcbnew.FromMM(mid_radius)
        end_radius = pcbnew.FromMM(end_radius)

        if debug:
            logging.info('util.points_around_vias: center = ({:g}, {:g})'.format(
                pcbnew.ToMM(center.x), pcbnew.ToMM(center.y)))
            print('util.points_around_vias: center = ({:g}, {:g})'.format(
                pcbnew.ToMM(center.x), pcbnew.ToMM(center.y)))
            for via in vias:
                via_position = util.to_point(via)
                print('util.points_around_vias: via = ({:g}, {:g})'.format(
                    pcbnew.ToMM(via_position.x), pcbnew.ToMM(via_position.y)))
                  
            print('util.points_around_vias: start_radius = {:g} mm'.format(
                pcbnew.ToMM(start_radius)))
            print('util.points_around_vias: mid_radius = {:g} mm'.format(
                pcbnew.ToMM(mid_radius)))
            print('util.points_around_vias: end_radius = {:g} mm'.format(
                pcbnew.ToMM(end_radius)))

        hypotenuse1 = (via1 - center).EuclideanNorm()
        adjacent1 = start_radius - mid_radius + hypotenuse1
        opposite1 = math.sqrt(hypotenuse1**2 - adjacent1**2)

        hypotenuse2 = (via2 - center).EuclideanNorm()
        adjacent2 = end_radius - mid_radius + hypotenuse2
        opposite2 = math.sqrt(hypotenuse2**2 - adjacent2**2)

        beta1 = abs((via1 - center).Angle())
        gamma1 = math.atan2(opposite1, adjacent1)
        alpha1 = math.pi/2 - beta1 - gamma1

        beta2 = abs((via2 - center).Angle())
        gamma2 = math.atan2(opposite2, adjacent2)
        alpha2 = math.pi/2 - beta2 - gamma2

        # print('points_around_vias: hypotenuse1 = {:g} mm'.format(
        #     pcbnew.ToMM(hypotenuse1)))
        # print('points_around_vias: adjacent1 = {:g} mm'.format(
        #     pcbnew.ToMM(adjacent1)))
        # print('points_around_vias: opposite1 = {:g} mm'.format(
        #     pcbnew.ToMM(opposite1)))
        # print('points_around_vias: hypotenuse2 = {:g} mm'.format(
        #     pcbnew.ToMM(hypotenuse2)))
        # print('points_around_vias: adjacent2 = {:g} mm'.format(
        #     pcbnew.ToMM(adjacent2)))
        # print('points_around_vias: opposite2 = {:g} mm'.format(
        #     pcbnew.ToMM(opposite2)))
        # print('points_around_vias: beta1 = {:g}°'.format(
        #     math.degrees(beta1)))
        # print('points_around_vias: gamma1 = {:g}°'.format(
        #     math.degrees(gamma1)))
        # print('points_around_vias: alpha1 = {:g}°'.format(
        #     math.degrees(alpha1)))
        # print('points_around_vias: beta2 = {:g}°'.format(
        #     math.degrees(beta2)))
        # print('points_around_vias: gamma2 = {:g}°'.format(
        #     math.degrees(gamma2)))
        # print('points_around_vias: alpha2 = {:g}°'.format(
        #     math.degrees(alpha2)))

        points = []

        start11 = center + pcbnew.VECTOR2I(0, -start_radius)
        points11 = util.points_on_arc(
            center, start11, alpha1, self.segment_count(alpha1))
        points.extend(points11)

        start12 = via1 + (via1 - center).Resize(mid_radius - hypotenuse1)
        points12 = util.points_on_arc(
            via1, start12, -gamma1, self.segment_count(gamma1))
        points.extend(points12[::-1])

        points13 = util.points_on_arc(
            center, start12, beta1 + beta2, self.segment_count(beta1 + beta2))
        points.extend(points13[1:-1])

        start22 = via2 + (via2 - center).Resize(mid_radius - hypotenuse2)
        points22 = util.points_on_arc(
            via2, start22, gamma2, self.segment_count(gamma2))
        points.extend(points22)

        start21 = center + pcbnew.VECTOR2I(0, end_radius).Rotate(-alpha2)
        points21 = util.points_on_arc(
            center, start21, alpha2, self.segment_count(alpha2))
        points.extend(points21)
        
        return points
